# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.2.5] - 2024-12-17

### Fixed

-   Empty alerts datasource ([waze-ccp#10](https://gitlab.com/operator-ict/golemio/code/modules/waze-ccp/-/issues/10))

## [1.2.4] - 2024-10-16

### Added

-   AsyncAPI documentation ([integration-engine#262](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/262))

## [1.2.3] - 2024-09-12

### Changed

-   `.gitlab-ci.yml` cleanup ([devops#320](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/issues/320))

## [1.2.2] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.2.1] - 2024-06-17

### Removed

-   remove view waze_ccp.v_waze_potholes_last_days (issue: https://gitlab.com/operator-ict/golemio/projekty/tsk/potholes/-/issues/12)

## [1.2.0] - 2024-06-03

### Added

-   add cache-control header to all responses ([core#106](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/106))

### Removed

-   remove redis useCacheMiddleware ([core#106](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/106))

## [1.1.13] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.1.12] - 2024-04-08

### Changed

-   axios exchanged for native fetch ([core#99](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/99))

## [1.1.11] - 2024-03-25

### Fixed

-   Fixed router query validation rules ([core#93](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/93))

## [1.1.10] - 2024-02-21

### Removed

-   backup tables after bigint -> timestamp and partitioning migration changes ([waze-ccp#8](https://gitlab.com/operator-ict/golemio/code/modules/waze-ccp/-/issues/8))

## [1.1.9] - 2023-11-29

### Added

-   OG for potholes ([potholes#11](https://gitlab.com/operator-ict/golemio/projekty/tsk/potholes/-/issues/11))

## [1.1.8] - 2023-11-27

### Added

-   New alerts table with duplicity counters ([waze-ccp#8](https://gitlab.com/operator-ict/golemio/code/modules/waze-ccp/-/issues/8))

## [1.1.7] - 2023-11-15

### Changed

-   Replace bigint timestamps ([waze-ccp#6](https://gitlab.com/operator-ict/golemio/code/modules/waze-ccp/-/issues/6))

## [1.1.6] - 2023-10-16

### Changed

-   Remove duplicates [(tsk#9bb)](https://gitlab.com/operator-ict/golemio/projekty/tsk/potholes/-/issues/9)

## [1.1.5] - 2023-07-31

### Changed

-   Replaced moment.js with native Date ([core#68](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/68))

## [1.1.4] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.1.3] - 2023-03-08

## Changed

-   Change from mongoosed based validations to JSON Schema

## [1.1.2] - 2023-03-06

### Changed

-   Limit waze alerts records [(waze-cpp#5)](https://gitlab.com/operator-ict/golemio/code/modules/waze-ccp/-/issues/5)

## [1.1.1] - 2023-02-27

### Changed

-   Limit waze alerts records [(waze-cpp#5)](https://gitlab.com/operator-ict/golemio/code/modules/waze-ccp/-/issues/5)
-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.0.4] - 2022-11-29

### Changed

-   Update gitlab-ci template

## [1.0.3] - 2022-05-25

### Added

-   Move database migrations from schema-definitions (schema public) to the module (schema waze_ccp) ([waze-ccp#2](https://gitlab.com/operator-ict/golemio/code/modules/waze-ccp/-/issues/2))

