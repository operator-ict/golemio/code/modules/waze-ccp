import crypto from "crypto";
import { IInputAlertsInfo } from "#sch/datasources/interfaces/IInputAlerts";

export default class AlertsIdHelper {
    // PSQL:
    // md5(concat_ws(
    //     ',', pub_millis, road_type, location->>'x', location->>'y', street, city, country,
    //     type, subtype, report_by_municipality_user::text,jam_uuid
    // ))::uuid
    public static GenerateUuid(alert: IInputAlertsInfo) {
        const hashString = [
            alert.pubMillis?.toString(),
            alert.roadType,
            alert.location?.x,
            alert.location?.y,
            alert.street,
            alert.city,
            alert.country,
            alert.type,
            alert.subtype,
            alert.reportByMunicipalityUser === "true",
            alert.jamUuid,
        ]
            .filter((el) => el !== undefined)
            .join(",");
        const md5 = this.getMd5(hashString);
        return this.formatAsUuid(md5);
    }

    private static getMd5(text: string) {
        return crypto.createHash("md5").update(text).digest("hex");
    }

    private static formatAsUuid(text: string) {
        if (text.length !== 32) throw new Error(`AlertsIdHelper: Unable to format string '${text}' as uuid.`);
        return `${text.slice(0, 8)}-${text.slice(8, 12)}-${text.slice(12, 16)}-${text.slice(16, 20)}-${text.slice(20)}`;
    }
}
