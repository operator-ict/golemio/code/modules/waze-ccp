import { WazeAlertsRepository } from "#ie/repositories/WazeAlertsRepository";
import { WazeCCP } from "#sch/index";
import { IrregularitiesDto } from "#sch/models/IrregularityDto";
import { JamDto } from "#sch/models/JamDto";
import { config } from "@golemio/core/dist/integration-engine/config";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { WazeCCPAlertsTransformation, WazeCCPIrregularitiesTransformation, WazeCCPJamsTransformation } from "./";
import { log } from "@golemio/core/dist/integration-engine/helpers";

export class WazeCCPWorker extends BaseWorker {
    private dataSourceAlerts: DataSource;
    private dataSourceIrregularities: DataSource;
    private dataSourceJams: DataSource;
    private modelAlerts: WazeAlertsRepository;
    private modelIrregularities: PostgresModel;
    private modelJams: PostgresModel;
    private transformationIrregularities: WazeCCPIrregularitiesTransformation;
    private transformationJams: WazeCCPJamsTransformation;
    private queuePrefix: string;

    constructor() {
        super();

        this.dataSourceAlerts = new DataSource(
            WazeCCP.alerts.name + "DataSource",
            new HTTPFetchProtocolStrategy({
                headers: {},
                method: "GET",
                url: config.datasources.WazeCCP + "&types=alerts",
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(WazeCCP.alerts.name + "DataSource", WazeCCP.alerts.datasourceJsonSchema)
        );
        this.dataSourceIrregularities = new DataSource(
            WazeCCP.irregularities.name + "DataSource",
            new HTTPFetchProtocolStrategy({
                headers: {},
                method: "GET",
                url: config.datasources.WazeCCP + "&types=irregularities",
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(WazeCCP.irregularities.name + "DataSource", WazeCCP.irregularities.datasourceJsonSchema)
        );
        this.dataSourceJams = new DataSource(
            WazeCCP.jams.name + "DataSource",
            new HTTPFetchProtocolStrategy({
                headers: {},
                method: "GET",
                url: config.datasources.WazeCCP + "&types=traffic",
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(WazeCCP.jams.name + "DataSource", WazeCCP.jams.datasourceJsonSchema)
        );

        this.transformationIrregularities = new WazeCCPIrregularitiesTransformation();
        this.transformationJams = new WazeCCPJamsTransformation();

        this.modelAlerts = new WazeAlertsRepository();
        this.modelIrregularities = new PostgresModel(
            WazeCCP.irregularities.name + "Model",
            {
                outputSequelizeAttributes: IrregularitiesDto.attributeModel,
                pgTableName: WazeCCP.irregularities.pgTableName,
                pgSchema: WazeCCP.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(WazeCCP.irregularities.name + "ModelValidator", IrregularitiesDto.jsonSchema)
        );
        this.modelJams = new PostgresModel(
            WazeCCP.jams.name + "Model",
            {
                outputSequelizeAttributes: JamDto.attributeModel,
                pgTableName: WazeCCP.jams.pgTableName,
                pgSchema: WazeCCP.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(WazeCCP.jams.name + "ModelValidator", JamDto.jsonSchema)
        );

        this.queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + WazeCCP.name.toLowerCase();
    }

    public refreshAlertsInDB = async (msg: any): Promise<void> => {
        const data = await this.dataSourceAlerts.getAll();
        if (!data.alerts) {
            log.warn(`${WazeCCP.alerts.name}: Data source returned empty data.`);
            return;
        }
        const now = new Date();
        const transformationAlerts = new WazeCCPAlertsTransformation(now);
        const transformedData = transformationAlerts.transformArray(data.alerts);
        await this.modelAlerts.saveAlerts(transformedData);
    };

    public refreshIrregularitiesInDB = async (msg: any): Promise<void> => {
        const data = await this.dataSourceIrregularities.getAll();
        // enrich data by downloadedAt unix timestamp
        const dataWithDownloadAt = { ...data, downloadedAt: new Date() };
        const transformedData = await this.transformationIrregularities.transform(dataWithDownloadAt);
        await this.modelIrregularities.saveBySqlFunction(transformedData, ["id"]);
    };

    public refreshJamsInDB = async (msg: any): Promise<void> => {
        const data = await this.dataSourceJams.getAll();
        // enrich data by downloadedAt unix timestamp
        const dataWithDownloadAt = { ...data, downloadedAt: new Date() };
        const transformedData = await this.transformationJams.transform(dataWithDownloadAt);
        await this.modelJams.saveBySqlFunction(transformedData, ["id", "pub_utc_date"]);
    };

    public refreshAllDataInDB = async (msg: any): Promise<void> => {
        const queueNames: string[] = ["refreshAlertsInDB", "refreshIrregularitiesInDB", "refreshJamsInDB"];

        await Promise.all(
            queueNames.map((queueName: string) => {
                return this.sendMessageToExchange("workers." + this.queuePrefix + "." + queueName, "Just do it!");
            })
        );
    };
}
