import { IWazeAlerts } from "#ie/interfaces/WazeAlertsInterface";
import { WazeCCP } from "#sch";
import { AlertDto } from "#sch/models/AlertDto";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class WazeAlertsRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            WazeCCP.alerts.name + "Repository",
            {
                outputSequelizeAttributes: AlertDto.attributeModel,
                pgTableName: WazeCCP.alerts.pgTableName,
                pgSchema: WazeCCP.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(WazeCCP.alerts.name + "Validator", AlertDto.jsonSchema)
        );
    }

    public saveAlerts = async (data: IWazeAlerts[]) => {
        for (const item of data) {
            await this.saveAlert(item);
        }
    };

    private saveAlert = async (data: IWazeAlerts) => {
        if (await this.validate([data])) {
            const record = await this.sequelizeModel.findOne({
                where: {
                    id: data.id,
                    uuid: data.uuid,
                },
                order: [["downloaded_at", "DESC"]],
            });
            if (record) {
                return this.update(
                    {
                        duplicate_count: record.duplicate_count + 1,
                        downloaded_at: data.downloaded_at,
                        updated_at: data.downloaded_at,
                    },
                    { where: { uuid: data.uuid, id: data.id } }
                );
            }

            return this.sequelizeModel.create({ ...data, duplicate_count: 0, valid_from: data.downloaded_at });
        }
    };
}
