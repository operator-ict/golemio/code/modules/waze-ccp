import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { WazeCCP } from "#sch/index";
import { IWazeAlerts } from "#ie/interfaces/WazeAlertsInterface";
import { IInputAlertsInfo } from "#sch/datasources/interfaces/IInputAlerts";
import AlertsIdHelper from "#ie/helpers/AlertsIdHelper";

export class WazeCCPAlertsTransformation extends AbstractTransformation<IInputAlertsInfo, IWazeAlerts> {
    public name = WazeCCP.alerts.name + "Transformation";
    private readonly downloadedAt: Date;

    constructor(timestamp: Date) {
        super();
        this.downloadedAt = timestamp;
    }

    protected transformInternal = (alert: IInputAlertsInfo): IWazeAlerts => {
        return {
            city: alert.city ?? null,
            confidence: alert.confidence ?? null,
            country: alert.country ?? null,
            downloaded_at: this.downloadedAt,
            id: AlertsIdHelper.GenerateUuid(alert),
            jam_uuid: alert.jamUuid ?? null,
            location: alert.location,
            magvar: alert.magvar ?? null,
            pub_millis: alert.pubMillis?.toString(),
            pub_utc_date: new Date(+alert.pubMillis),
            reliability: alert.reliability ?? null,
            report_by_municipality_user: alert.reportByMunicipalityUser === "true",
            report_description: alert.reportDescription ?? null,
            report_rating: alert.reportRating ?? null,
            road_type: alert.roadType ?? null,
            street: alert.street ?? null,
            subtype: alert.subtype ?? null,
            thumbs_up: alert.nThumbsUp ?? null,
            type: alert.type ?? null,
            uuid: alert.uuid,
        };
    };
}
