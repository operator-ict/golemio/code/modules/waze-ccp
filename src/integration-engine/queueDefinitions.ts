import { config } from "@golemio/core/dist/integration-engine/config";
import { IQueueDefinition } from "@golemio/core/dist/integration-engine/queueprocessors";
import { WazeCCP } from "#sch/index";
import { WazeCCPWorker } from "#ie/WazeCCPWorker";

const queueDefinitions: IQueueDefinition[] = [
    {
        name: WazeCCP.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + WazeCCP.name.toLowerCase(),
        queues: [
            {
                name: "refreshAllDataInDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 4 * 60 * 1000, // 4 minutes
                },
                worker: WazeCCPWorker,
                workerMethod: "refreshAllDataInDB",
            },
            {
                name: "refreshAlertsInDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 90 * 1000, // 90 seconds
                },
                worker: WazeCCPWorker,
                workerMethod: "refreshAlertsInDB",
            },
            {
                name: "refreshIrregularitiesInDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 4 * 60 * 1000, // 4 minutes
                },
                worker: WazeCCPWorker,
                workerMethod: "refreshIrregularitiesInDB",
            },
            {
                name: "refreshJamsInDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 4 * 60 * 1000, // 4 minutes
                },
                worker: WazeCCPWorker,
                workerMethod: "refreshJamsInDB",
            },
        ],
    },
];

export { queueDefinitions };
