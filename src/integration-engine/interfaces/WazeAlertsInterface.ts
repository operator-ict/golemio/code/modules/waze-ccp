export interface IWazeAlerts {
    id: string;
    city: string | null;
    confidence: number | null;
    country: string | null;
    downloaded_at: Date;
    jam_uuid: string | null;
    location: {
        x: number;
        y: number;
    };
    magvar: number | null;
    pub_millis: string; // milliseconds
    pub_utc_date: Date;
    reliability: number | null;
    report_by_municipality_user: boolean;
    report_description: string | null;
    report_rating: number | null;
    road_type: number | null;
    street: string | null;
    subtype: string | null;
    thumbs_up: number | null;
    type: string | null;
    uuid: string;
}
