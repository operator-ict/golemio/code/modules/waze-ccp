import hash from "object-hash";

const computeHash = (obj: object): string => hash(obj, { unorderedArrays: true });

const generateAJIUniqueIdentifierHash = (obj: object, rootStartTime: number): string => {
    const objHash = (this as any).computeHash(obj);
    const combinedObject = {
        originalObjectHash: objHash,
        rootTime: rootStartTime,
    };
    return (this as any).computeHash(combinedObject);
};

export { computeHash, generateAJIUniqueIdentifierHash };
