import { dateTime } from "@golemio/core/dist/helpers/DateTime";

export class ParseDateTimeHelper {
    public static getDateTimeSeparately(datetime: Date) {
        const wrappedDate = dateTime(datetime, { timeZone: "Europe/Prague" });
        const date = wrappedDate.format("yyyy-LL-dd");
        const time = wrappedDate.format("HH:mm:ss");

        return { date, time };
    }
}
