import { IPotholesController } from "#og/domain/IPotholesController";
import { IPotholesParams } from "#og/domain/IPotholesParams";
import { IPotholesRepository } from "#og/domain/IPotholesRepository";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { IAlertDto } from "#sch/interfaces/IAlertDto";
import { dateTime } from "@golemio/core/dist/helpers/DateTime";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { IGeoJSONFeature, buildGeojsonFeatureCollection } from "@golemio/core/dist/output-gateway";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class PotholesController implements IPotholesController {
    constructor(
        @inject(ModuleContainerToken.PotholesRepository) private repository: IPotholesRepository,
        @inject(ModuleContainerToken.PotholesTransformation)
        private transformation: AbstractTransformation<IAlertDto, IGeoJSONFeature>
    ) {}

    public getAll = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const params = await this.parseParams(req);
            const result = await this.repository.GetAll(params);

            const transformedResult = buildGeojsonFeatureCollection(this.transformation.transformArray(result));

            res.json(transformedResult);
        } catch (err) {
            next(err);
        }
    };

    private async parseParams(req: Request): Promise<Partial<IPotholesParams>> {
        try {
            const result: Partial<IPotholesParams> = {
                limit: req.query.limit ? Number(req.query.limit) : undefined,
                offset: req.query.offset ? Number(req.query.offset) : undefined,
                dateFrom: req.query.from ? (req.query.from as string) : dateTime(new Date()).subtract(7, "days").toISOString(),
                dateTo: req.query.to ? (req.query.to as string) : new Date().toISOString(),
            };

            return result;
        } catch (error) {
            throw new GeneralError("Param parsing error", PotholesController.name, error, 500);
        }
    }
}
