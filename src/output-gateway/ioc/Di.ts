import { PotholesController } from "#og/controllers/PotholesController";
import { PotholesRepository } from "#og/repositories/PotholesRepository";
import { PotholesTransformation } from "#og/transformation/PotholesDtoTransformation";
import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/Di";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { ModuleContainerToken } from "./ModuleContainerToken";

//container
const potHolesContainer: DependencyContainer = OutputGatewayContainer.createChildContainer();

//#region Repositories
potHolesContainer.registerSingleton(ModuleContainerToken.PotholesRepository, PotholesRepository);
//#endregion

//#region Transformations
potHolesContainer.registerSingleton(ModuleContainerToken.PotholesTransformation, PotholesTransformation);
//#endregion

//#region Controllers
potHolesContainer.registerSingleton(ModuleContainerToken.PotholesController, PotholesController);
//#endregion

export { potHolesContainer as PotHolesContainer };
