const ModuleContainerToken = {
    PotholesRepository: Symbol(),
    PotholesTransformation: Symbol(),
    PotholesController: Symbol(),
};

export { ModuleContainerToken };
