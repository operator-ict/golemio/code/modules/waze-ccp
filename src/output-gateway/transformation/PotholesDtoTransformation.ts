import { IPothole } from "#og/domain/IPothole";
import { IPotholesOutputDto } from "#og/domain/IPotholesOutputDto";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { IGeoJSONFeature, buildGeojsonFeature } from "@golemio/core/dist/output-gateway";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class PotholesTransformation extends AbstractTransformation<IPothole, IGeoJSONFeature> {
    public name: string = "PotholesTransformation";

    protected transformInternal = (element: IPothole) => {
        const properties: IPotholesOutputDto = {
            uuid: element.uuid,
            city: element.city ?? null,
            street: element.street ?? null,
            road_type: element.road_type_name ?? null,
            event_direction: element.magvar ?? null,
            published_at: element.published_at,
            valid_from: element.valid_from,
            last_reported_at: element.last_reported,
            reliability: element.reliability ?? null,
            confidence: element.confidence ?? null,
            duplicate_count: element.duplicate_count ?? null,
        };

        return buildGeojsonFeature({ ...properties, lon: element.location.x, lat: element.location.y }, "lon", "lat", true);
    };
}
