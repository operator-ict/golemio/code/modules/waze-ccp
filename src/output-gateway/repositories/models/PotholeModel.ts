import { IPothole } from "#og/domain/IPothole";
import { AlertDto } from "#sch/models/AlertDto";
import { DataTypes } from "@golemio/core/dist/shared/sequelize";

export class PotholeModel extends AlertDto implements IPothole {
    declare road_type_name: string | null;
    declare published_at: Date;
    declare last_reported: Date;

    public static attributeModel = {
        ...AlertDto.attributeModel,
        road_type_name: { type: DataTypes.TEXT },
        published_at: { type: DataTypes.DATE, allowNull: false },
        last_reported: { type: DataTypes.DATE, allowNull: false },
    };
}
