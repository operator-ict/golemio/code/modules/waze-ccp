import { IPothole } from "#og/domain/IPothole";
import { IPotholesRepository } from "#og/domain/IPotholesRepository";
import { WazeCCP } from "#sch/index";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import Sequelize, { Op } from "@golemio/core/dist/shared/sequelize";
import { IPotholesParams } from "../domain/IPotholesParams";
import { PotholeModel } from "./models/PotholeModel";

export class PotholesRepository extends SequelizeModel implements IPotholesRepository {
    constructor() {
        super("PotholesRepository", "v_waze_potholes_v2", PotholeModel.attributeModel, {
            schema: WazeCCP.pgSchema,
        });
    }

    public GetAll(params: Partial<IPotholesParams>): Promise<IPothole[]> {
        try {
            const { whereAttributes, order } = this.prepareWhereAndOrder(params);

            const bindParams = {
                dateFrom: params.dateFrom,
                dateTo: params.dateTo,
            };

            return this.sequelizeModel.findAll<PotholeModel>({
                attributes: {
                    exclude: [
                        "pub_millis",
                        "pub_utc_date",
                        "country",
                        "report_description",
                        "report_rating",
                        "type",
                        "subtype",
                        "report_by_municipality_user",
                        "thumbs_up",
                        "jam_uuid",
                        "downloaded_at",
                        "create_batch_id",
                        "created_at",
                        "created_by",
                        "update_batch_id",
                        "updated_by",
                    ],
                },
                where: whereAttributes,
                limit: params.limit,
                offset: params.offset,
                order,
                bind: bindParams,
                raw: true,
            });
        } catch (err) {
            throw new GeneralError("Database error ~ GetAll Potholes", this.name, err, 500);
        }
    }

    private prepareWhereAndOrder(params: Partial<IPotholesParams>) {
        let whereAttributes: Sequelize.WhereOptions = {
            subtype: "HAZARD_ON_ROAD_POT_HOLE",
        };
        const order: Sequelize.OrderItem[] = [];

        order.push([Sequelize.col("uuid"), "asc"]);
        order.push([Sequelize.col("pub_utc_date"), "desc"]);

        const literalConditions = [];
        if (params.dateFrom) {
            literalConditions.push(Sequelize.literal(`(pub_utc_date > $dateFrom)`));
        }

        if (params.dateTo) {
            literalConditions.push(Sequelize.literal(`(pub_utc_date <= $dateTo)`));
        }

        if (literalConditions.length > 0) {
            whereAttributes = {
                ...whereAttributes,
                [Op.and]: literalConditions,
            };
        }

        return { whereAttributes, order };
    }

    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
}
