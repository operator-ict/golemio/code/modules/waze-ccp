import { IPotholesController } from "#og/domain/IPotholesController";
import { PotHolesContainer } from "#og/ioc/Di";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway/CacheHeaderMiddleware";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { query } from "@golemio/core/dist/shared/express-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
@injectable()
export class PotHolesRouter extends AbstractRouter {
    private controller: IPotholesController;
    protected cacheHeaderMiddleware: CacheHeaderMiddleware;

    constructor() {
        super("v1", "potholes");
        this.controller = PotHolesContainer.resolve<IPotholesController>(ModuleContainerToken.PotholesController);
        this.cacheHeaderMiddleware = PotHolesContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
        this.initRoutes();
    }

    protected initRoutes(): void {
        this.router.get(
            "/data",
            [query("from").optional().isISO8601().not().isArray(), query("to").optional().isISO8601().not().isArray()],
            pagination,
            checkErrors,
            paginationLimitMiddleware(this.path),
            this.cacheHeaderMiddleware.getMiddleware(3 * 60, 60),
            this.controller.getAll
        );
    }
}

const v1PotHolesRouter: AbstractRouter = new PotHolesRouter();

export { v1PotHolesRouter };
