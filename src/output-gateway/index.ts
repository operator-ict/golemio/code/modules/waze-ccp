import { v1PotHolesRouter } from "./routers/PotHolesRouter";

export * from "./routers/PotHolesRouter";

export const routers = [v1PotHolesRouter];
