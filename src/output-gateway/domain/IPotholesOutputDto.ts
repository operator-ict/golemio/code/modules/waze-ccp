export interface IPotholesOutputDto {
    uuid: string;
    city: string | null;
    street: string | null;
    road_type: string | null;
    event_direction: number | null;
    published_at: Date;
    valid_from: Date;
    last_reported_at: Date;
    reliability: number | null;
    confidence: number | null;
    duplicate_count: number | null;
}
