export interface IPotholesParams {
    limit: number;
    offset: number;
    dateFrom: string;
    dateTo: string;
}
