import { IAlertDto } from "#sch/interfaces/IAlertDto";
import { IPotholesParams } from "./IPotholesParams";

export interface IPotholesRepository {
    GetAll(params: Partial<IPotholesParams>): Promise<IAlertDto[]>;
    GetOne(id: string): Promise<IAlertDto>;
}
