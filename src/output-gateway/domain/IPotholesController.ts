import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
export interface IPotholesController {
    getAll(req: Request, res: Response, next: NextFunction): Promise<void>;
}
