import { IAlertDto } from "#sch/interfaces/IAlertDto";

export interface IPothole extends IAlertDto {
    road_type_name: string | null;
    published_at: Date;
    last_reported: Date;
}
