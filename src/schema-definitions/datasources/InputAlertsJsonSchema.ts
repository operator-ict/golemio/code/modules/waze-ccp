import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IInputAlerts } from "#sch/datasources/interfaces/IInputAlerts";

export const InputAlertsJsonSchema: JSONSchemaType<IInputAlerts> = {
    type: "object",
    properties: {
        alerts: {
            type: "array",
            items: {
                type: "object",
                properties: {
                    city: { type: "string" },
                    confidence: { type: "number" },
                    country: { type: "string" },
                    jamUuid: { type: "string" },
                    location: {
                        type: "object",
                        properties: {
                            x: { type: "number" },
                            y: { type: "number" },
                        },
                        required: ["x", "y"],
                    },
                    magvar: { type: "number" },
                    nThumbsUp: { type: "number" },
                    pubMillis: { type: "number" },
                    reliability: { type: "number" },
                    reportByMunicipalityUser: { type: "string" },
                    reportDescription: { type: "string" },
                    reportRating: { type: "number" },
                    roadType: { type: "number" },
                    street: { type: "string" },
                    subtype: { type: "string" },
                    type: { type: "string" },
                    uuid: { type: "string" },
                },
                required: ["pubMillis", "uuid"],
            },
        },
        endTime: { type: "string" },
        endTimeMillis: { type: "number" },
        startTime: { type: "string" },
        startTimeMillis: { type: "number" },
    },
    required: ["startTimeMillis"],
};
