export interface IInputAlertsInfo {
    city: string;
    confidence: number;
    country: string;
    jamUuid: string;
    location: {
        x: number;
        y: number;
    };
    magvar: number;
    nThumbsUp: number;
    pubMillis: number;
    reliability: number;
    reportByMunicipalityUser: string;
    reportDescription: string;
    reportRating: number;
    roadType: number;
    street: string;
    subtype: string;
    type: string;
    uuid: string;
}

export interface IInputAlerts {
    alerts: IInputAlertsInfo[];
    endTime: string;
    endTimeMillis: number;
    startTime: string;
    startTimeMillis: number;
}
