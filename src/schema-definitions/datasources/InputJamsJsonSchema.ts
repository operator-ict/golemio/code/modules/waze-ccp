export const InputJamsJsonSchema = {
    type: "object",
    properties: {
        endTime: { type: "string" },
        endTimeMillis: { type: "number" },
        jams: {
            type: "array",
            items: {
                type: "object",
                properties: {
                    blockingAlertUuid: { type: "string" },
                    city: { type: "string" },
                    country: { type: "string" },
                    delay: { type: "number" },
                    endNode: { type: "string" },
                    length: { type: "number" },
                    level: { type: "number" },
                    line: {
                        type: "array",
                        items: {
                            type: "object",
                            properties: { x: { type: "number" }, y: { type: "number" } },
                        },
                    },
                    pubMillis: { type: "number" },
                    roadType: { type: "number" },
                    segments: {
                        type: "array",
                        items: {
                            type: "object",
                        },
                    },
                    speed: { type: "number" },
                    speedKMH: { type: "number" },
                    street: { type: "string" },
                    turnType: { type: "string" },
                    type: { type: "string" },
                    uuid: { type: "number" },
                },
                required: ["pubMillis", "uuid"],
            },
        },
        startTime: { type: "string" },
        startTimeMillis: { type: "number" },
    },
    required: ["startTimeMillis"],
};
