export interface IIrregularityDto {
    alerts_count: number | null;
    cause_type: string | null;
    city: string | null;
    country: string | null;
    delay_seconds: number | null;
    detection_date: string | null;
    detection_date_millis: number;
    detection_utc_date: Date | null;
    downloaded_at: Date | null;
    drivers_count: number | null;
    end_node: string | null;
    id: string;
    is_highway: boolean | null;
    jam_level: number | null;
    length: number | null;
    line: Array<{
        x: number;
        y: number;
    }> | null;
    n_comments: number | null;
    n_images: number | null;
    n_thumbs_up: number | null;
    regular_speed: number | null;
    seconds: number | null;
    severity: number | null;
    speed: number | null;
    start_node: string | null;
    street: string | null;
    trend: number | null;
    type: string | null;
    update_date: string | null;
    update_date_millis: number;
    update_utc_date: Date | null;
    uuid: number;
}
