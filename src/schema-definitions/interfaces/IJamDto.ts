export interface IJamDto {
    blocking_alert_id: string | null;
    city: string | null;
    country: string | null;
    delay: number | null;
    downloaded_at: Date | null;
    end_node: string | null;
    id: string;
    length: number | null;
    level: number | null;
    line: Array<{
        x: number;
        y: number;
    }> | null;
    pub_millis: number;
    pub_utc_date: Date;
    road_type: number | null;
    speed: number | null;
    speed_kmh: number | null;
    start_node: string | null;
    street: string | null;
    turn_line: {
        x: number;
        y: number;
    } | null;
    turn_type: string | null;
    type: string | null;
    uuid: number;
}
