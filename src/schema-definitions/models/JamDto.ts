import { IJamDto } from "#sch/interfaces/IJamDto";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";

export class JamDto extends Model<IJamDto> implements IJamDto {
    declare blocking_alert_id: string | null;
    declare city: string | null;
    declare country: string | null;
    declare delay: number | null;
    declare downloaded_at: Date | null;
    declare end_node: string | null;
    declare id: string;
    declare length: number | null;
    declare level: number | null;
    declare line: Array<{ x: number; y: number }> | null;
    declare pub_millis: number;
    declare pub_utc_date: Date;
    declare road_type: number | null;
    declare speed: number | null;
    declare speed_kmh: number | null;
    declare start_node: string | null;
    declare street: string | null;
    declare turn_line: { x: number; y: number } | null;
    declare turn_type: string | null;
    declare type: string | null;
    declare uuid: number;

    public static attributeModel: ModelAttributes<JamDto> = {
        blocking_alert_id: DataTypes.TEXT,
        city: DataTypes.TEXT,
        country: DataTypes.TEXT,
        delay: DataTypes.INTEGER,
        downloaded_at: DataTypes.DATE,
        end_node: DataTypes.TEXT,
        id: { type: DataTypes.STRING, primaryKey: true, allowNull: false },
        length: DataTypes.INTEGER,
        level: DataTypes.INTEGER,
        line: DataTypes.JSONB,
        pub_millis: { type: DataTypes.BIGINT, allowNull: false },
        pub_utc_date: DataTypes.DATE,
        road_type: DataTypes.INTEGER,
        speed: DataTypes.REAL,
        speed_kmh: DataTypes.REAL,
        start_node: DataTypes.TEXT,
        street: DataTypes.TEXT,
        turn_line: DataTypes.JSONB,
        turn_type: DataTypes.TEXT,
        type: DataTypes.TEXT,
        uuid: { type: DataTypes.STRING, allowNull: false },
    };

    public static jsonSchema: JSONSchemaType<IJamDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                blocking_alert_id: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                city: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                country: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                delay: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                downloaded_at: {
                    oneOf: [
                        { type: "object", required: ["toISOString"] },
                        { type: "null", nullable: true },
                    ],
                },
                end_node: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                id: { type: "string" },
                length: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                level: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                line: {
                    oneOf: [
                        {
                            type: "array",
                            items: {
                                type: "object",
                                properties: { x: { type: "number" }, y: { type: "number" } },
                                required: ["x", "y"],
                            },
                        },
                        { type: "null", nullable: true },
                    ],
                },
                pub_millis: { type: "integer" },
                pub_utc_date: { type: "object", required: ["toISOString"] },
                road_type: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                speed: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                speed_kmh: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                start_node: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                street: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                turn_line: {
                    oneOf: [
                        {
                            type: "object",
                            properties: { x: { type: "number" }, y: { type: "number" } },
                            required: ["x", "y"],
                        },
                        { type: "null", nullable: true },
                    ],
                },
                turn_type: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                type: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                uuid: { type: "integer" },
            },
            required: ["id", "pub_millis", "pub_utc_date", "uuid"],
        },
    };
}
