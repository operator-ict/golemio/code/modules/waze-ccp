import { IIrregularityDto } from "#sch/interfaces/IIrregularityDto";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";

export class IrregularitiesDto extends Model<IIrregularityDto> implements IIrregularityDto {
    declare alerts_count: number | null;
    declare cause_type: string | null;
    declare city: string | null;
    declare country: string | null;
    declare delay_seconds: number | null;
    declare detection_date: string | null;
    declare detection_date_millis: number;
    declare detection_utc_date: Date | null;
    declare downloaded_at: Date | null;
    declare drivers_count: number | null;
    declare end_node: string | null;
    declare id: string;
    declare is_highway: boolean | null;
    declare jam_level: number | null;
    declare length: number | null;
    declare line: Array<{ x: number; y: number }> | null;
    declare n_comments: number | null;
    declare n_images: number | null;
    declare n_thumbs_up: number | null;
    declare regular_speed: number | null;
    declare seconds: number | null;
    declare severity: number | null;
    declare speed: number | null;
    declare start_node: string | null;
    declare street: string | null;
    declare trend: number | null;
    declare type: string | null;
    declare update_date: string | null;
    declare update_date_millis: number;
    declare update_utc_date: Date | null;
    declare uuid: number;

    public static attributeModel: ModelAttributes<IrregularitiesDto> = {
        alerts_count: DataTypes.INTEGER,
        cause_type: DataTypes.TEXT,
        city: DataTypes.TEXT,
        country: DataTypes.TEXT,
        delay_seconds: DataTypes.INTEGER,
        detection_date: DataTypes.TEXT,
        detection_date_millis: { type: DataTypes.BIGINT, allowNull: false },
        detection_utc_date: DataTypes.DATE,
        downloaded_at: DataTypes.DATE,
        drivers_count: DataTypes.INTEGER,
        end_node: DataTypes.TEXT,
        id: { type: DataTypes.STRING, primaryKey: true, allowNull: false },
        is_highway: DataTypes.BOOLEAN,
        jam_level: DataTypes.INTEGER,
        length: DataTypes.INTEGER,
        line: DataTypes.JSONB,
        n_comments: DataTypes.INTEGER,
        n_images: DataTypes.INTEGER,
        n_thumbs_up: DataTypes.INTEGER,
        regular_speed: DataTypes.REAL,
        seconds: DataTypes.INTEGER,
        severity: DataTypes.REAL,
        speed: DataTypes.REAL,
        start_node: DataTypes.TEXT,
        street: DataTypes.TEXT,
        trend: DataTypes.INTEGER,
        type: DataTypes.TEXT,
        update_date: DataTypes.TEXT,
        update_date_millis: { type: DataTypes.BIGINT, allowNull: false },
        update_utc_date: DataTypes.DATE,
        uuid: { type: DataTypes.TEXT, allowNull: false },
    };

    public static jsonSchema: JSONSchemaType<IIrregularityDto[]> = {
        $schema: "http://json-schema.org/draft-04/schema#",
        title: "IrregularitiesDtoSchema",
        type: "array",
        items: {
            type: "object",
            properties: {
                alerts_count: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                cause_type: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                city: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                country: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                delay_seconds: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                detection_date: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                detection_date_millis: { type: "integer" },
                detection_utc_date: {
                    oneOf: [
                        { type: "object", required: ["toISOString"] },
                        { type: "null", nullable: true },
                    ],
                },
                downloaded_at: {
                    oneOf: [
                        { type: "object", required: ["toISOString"] },
                        { type: "null", nullable: true },
                    ],
                },
                drivers_count: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                end_node: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                id: { type: "string" },
                is_highway: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }] },
                jam_level: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                length: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                line: {
                    oneOf: [
                        {
                            type: "array",
                            items: {
                                type: "object",
                                properties: {
                                    x: { type: "number" },
                                    y: { type: "number" },
                                },
                                required: ["x", "y"],
                            },
                        },
                        { type: "null", nullable: true },
                    ],
                },
                n_comments: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                n_images: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                n_thumbs_up: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                regular_speed: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                seconds: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                severity: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                speed: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                start_node: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                street: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                trend: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                type: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                update_date: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                update_date_millis: { type: "integer" },
                update_utc_date: {
                    oneOf: [
                        { type: "object", required: ["toISOString"] },
                        { type: "null", nullable: true },
                    ],
                },
                uuid: { type: "integer" },
            },
            required: ["detection_date_millis", "id", "update_date_millis", "uuid"],
        },
    };
}
