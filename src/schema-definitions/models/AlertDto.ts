import { IAlertDto } from "#sch/interfaces/IAlertDto";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export class AlertDto extends Model<IAlertDto> implements IAlertDto {
    declare city: string | null;
    declare confidence: number | null;
    declare country: string | null;
    declare downloaded_at: Date;
    declare duplicate_count: number;
    declare id: string;
    declare jam_uuid: string | null;
    declare location: { x: number; y: number };
    declare magvar: number | null;
    declare pub_millis: string;
    declare pub_utc_date: Date;
    declare reliability: number | null;
    declare report_by_municipality_user: boolean;
    declare report_description: string | null;
    declare report_rating: number | null;
    declare road_type: number | null;
    declare street: string | null;
    declare subtype: string | null;
    declare thumbs_up: number | null;
    declare type: string | null;
    declare uuid: string;
    declare valid_from: Date;

    public static attributeModel: ModelAttributes<AlertDto> = {
        city: DataTypes.TEXT,
        confidence: DataTypes.INTEGER,
        country: DataTypes.TEXT,
        downloaded_at: { type: DataTypes.DATE, allowNull: false },
        duplicate_count: { type: DataTypes.INTEGER, allowNull: false },
        id: { type: DataTypes.UUID, allowNull: false, primaryKey: true },
        jam_uuid: DataTypes.TEXT,
        location: DataTypes.JSONB,
        magvar: DataTypes.INTEGER,
        pub_millis: { type: DataTypes.BIGINT, allowNull: false },
        pub_utc_date: { type: DataTypes.DATE, allowNull: false, primaryKey: true },
        reliability: DataTypes.INTEGER,
        report_by_municipality_user: DataTypes.BOOLEAN,
        report_description: DataTypes.TEXT,
        report_rating: DataTypes.INTEGER,
        road_type: DataTypes.INTEGER,
        street: DataTypes.TEXT,
        subtype: DataTypes.TEXT,
        thumbs_up: DataTypes.INTEGER,
        type: DataTypes.TEXT,
        uuid: { type: DataTypes.TEXT, allowNull: false, primaryKey: true },
        valid_from: { type: DataTypes.DATE, allowNull: false },
    };

    public static jsonSchema: JSONSchemaType<IAlertDto[]> = {
        $schema: "http://json-schema.org/draft-04/schema#",
        title: "AlertsDtoSchema",
        type: "array",
        items: {
            type: "object",
            properties: {
                city: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                confidence: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                country: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                downloaded_at: { type: "object", required: ["toISOString"] },
                duplicate_count: { type: "integer" },
                id: { type: "string" },
                jam_uuid: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                location: {
                    type: "object",
                    properties: { x: { type: "number" }, y: { type: "number" } },
                    required: ["x", "y"],
                },
                magvar: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                pub_millis: { type: "string" },
                pub_utc_date: { type: "object", required: ["toISOString"] },
                reliability: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                report_by_municipality_user: { type: "boolean" },
                report_description: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                report_rating: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                road_type: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                street: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                subtype: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                thumbs_up: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                type: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                uuid: { type: "string" },
                valid_from: { type: "object", required: ["toISOString"] },
            },
            required: ["pub_millis", "pub_utc_date", "uuid"],
        },
    };
}
