import { InputAlertsJsonSchema } from "#sch/datasources/InputAlertsJsonSchema";
import { InputJamsJsonSchema } from "#sch/datasources/InputJamsJsonSchema";
import { InputIrregularitiesJsonSchema } from "#sch/datasources/InputIrregularitiesJsonSchema";

const forExport: any = {
    name: "WazeCCP",
    pgSchema: "waze_ccp",
    alerts: {
        datasourceJsonSchema: InputAlertsJsonSchema,
        name: "WazeCCPAlerts",
        pgTableName: "wazeccp_alerts",
    },
    irregularities: {
        datasourceJsonSchema: InputIrregularitiesJsonSchema,
        name: "WazeCCPIrregularities",
        pgTableName: "wazeccp_irregularities",
    },
    jams: {
        datasourceJsonSchema: InputJamsJsonSchema,
        name: "WazeCCPJams",
        pgTableName: "wazeccp_jams",
    },
};

export { forExport as WazeCCP };
