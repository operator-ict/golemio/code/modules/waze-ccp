drop VIEW v_waze_potholes_last_days;

CREATE OR REPLACE VIEW v_waze_potholes_last_days
AS SELECT DISTINCT ON (wa.uuid) wa.uuid,
    wa.city,
    wa.location,
    wa.street,
    wa.road_type,
    wa.magvar AS event_direction,
    timezone('Europe/Prague'::text, wa.pub_utc_date) AS published_at,
    timezone('Europe/Prague'::text, wa.created_at) AS last_reported,
    wa.reliability,
    wa.report_rating,
    wa.confidence
   FROM wazeccp_alerts wa
  WHERE wa.subtype = 'HAZARD_ON_ROAD_POT_HOLE'::text AND wa.pub_utc_date > (now()::date - '7 days'::interval)
  ORDER BY wa.uuid, wa.pub_utc_date, wa.created_at DESC;

