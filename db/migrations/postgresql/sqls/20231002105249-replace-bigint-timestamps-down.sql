-- wazeccp_irregularities_backup -> wazeccp_irregularities
DROP TABLE wazeccp_irregularities;
ALTER TABLE wazeccp_irregularities_backup RENAME TO wazeccp_irregularities;

ALTER INDEX wazeccp_irregularities_backup_pkey RENAME TO wazeccp_irregularities_pkey;

-- wazeccp_jams_backup -> wazeccp_jams
DROP TABLE wazeccp_jams;
ALTER TABLE wazeccp_jams_backup RENAME TO wazeccp_jams;
ALTER TABLE wazeccp_jams_backup_min RENAME TO wazeccp_jams_min;
ALTER TABLE wazeccp_jams_backup_y2019 RENAME TO wazeccp_jams_y2019;
ALTER TABLE wazeccp_jams_backup_y2020 RENAME TO wazeccp_jams_y2020;
ALTER TABLE wazeccp_jams_backup_y2021 RENAME TO wazeccp_jams_y2021;
ALTER TABLE wazeccp_jams_backup_y2022 RENAME TO wazeccp_jams_y2022;
ALTER TABLE wazeccp_jams_backup_y2023 RENAME TO wazeccp_jams_y2023;
ALTER TABLE wazeccp_jams_backup_y2024 RENAME TO wazeccp_jams_y2024;
ALTER TABLE wazeccp_jams_backup_y2025 RENAME TO wazeccp_jams_y2025;
ALTER TABLE wazeccp_jams_backup_y2026 RENAME TO wazeccp_jams_y2026;
ALTER TABLE wazeccp_jams_backup_y2027max RENAME TO wazeccp_jams_y2027mxx;

ALTER INDEX wazeccp_jams_backup_pkey RENAME TO wazeccp_jams_pkey;
ALTER INDEX wazeccp_jams_backup_blocking_alert_id RENAME TO wazeccp_jams_blocking_alert_id;
ALTER INDEX wazeccp_jams_backup_created_at RENAME TO wazeccp_jams_created_at;
ALTER INDEX wazeccp_jams_backup_downloaded_at RENAME TO wazeccp_jams_downloaded_at;
ALTER INDEX wazeccp_jams_backup_pub_utc_date RENAME TO wazeccp_jams_pub_utc_date;

-- wazeccp_alerts_backup -> wazeccp_alerts
DROP VIEW v_waze_potholes_last_days;
DROP TABLE wazeccp_alerts;

ALTER TABLE wazeccp_alerts_backup RENAME TO wazeccp_alerts;
ALTER TABLE wazeccp_alerts_backup_min RENAME TO wazeccp_alerts_min;
ALTER TABLE wazeccp_alerts_backup_y2019 RENAME TO wazeccp_alerts_y2019;
ALTER TABLE wazeccp_alerts_backup_y2020 RENAME TO wazeccp_alerts_y2020;
ALTER TABLE wazeccp_alerts_backup_y2021 RENAME TO wazeccp_alerts_y2021;
ALTER TABLE wazeccp_alerts_backup_y2022 RENAME TO wazeccp_alerts_y2022;
ALTER TABLE wazeccp_alerts_backup_y2023 RENAME TO wazeccp_alerts_y2023;
ALTER TABLE wazeccp_alerts_backup_y2024 RENAME TO wazeccp_alerts_y2024;
ALTER TABLE wazeccp_alerts_backup_y2025 RENAME TO wazeccp_alerts_y2025;
ALTER TABLE wazeccp_alerts_backup_y2026 RENAME TO wazeccp_alerts_y2026;
ALTER TABLE wazeccp_alerts_backup_y2027max RENAME TO wazeccp_alerts_y2027max;

ALTER INDEX wazeccp_alerts_backup_pkey RENAME TO wazeccp_alerts_pkey;
ALTER INDEX wazeccp_alerts_backup_idx RENAME TO wazeccp_alerts_idx;
ALTER INDEX wazeccp_alerts_backup_downloaded_at_idx RENAME TO wazeccp_alerts_downloaded_at_idx;

CREATE VIEW v_waze_potholes_last_days
AS SELECT DISTINCT ON (wa.uuid) wa.uuid,
    wa.city,
    wa.location #>> '{}' as location,
    wa.street,
    wa.road_type,
    wa.magvar AS event_direction,
    timezone('Europe/Prague'::text, wa.pub_utc_date) AS published_at,
    timezone('Europe/Prague'::text, wa.created_at) AS last_reported,
    wa.reliability,
    wa.report_rating,
    wa.confidence
FROM wazeccp_alerts wa
WHERE wa.subtype = 'HAZARD_ON_ROAD_POT_HOLE'::text AND wa.pub_utc_date > (now()::date - '7 days'::interval)
ORDER BY wa.uuid, wa.pub_utc_date, wa.created_at DESC;
