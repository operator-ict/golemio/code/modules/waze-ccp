DROP VIEW v_waze_potholes_last_days;
DROP TABLE wazeccp_alerts;

-- wazeccp_alerts_old -> wazeccp_alerts
ALTER TABLE wazeccp_alerts_old RENAME TO wazeccp_alerts;
ALTER TABLE wazeccp_alerts_old_min RENAME TO wazeccp_alerts_min;
ALTER TABLE wazeccp_alerts_old_y2019 RENAME TO wazeccp_alerts_y2019;
ALTER TABLE wazeccp_alerts_old_y2020 RENAME TO wazeccp_alerts_y2020;
ALTER TABLE wazeccp_alerts_old_y2021 RENAME TO wazeccp_alerts_y2021;
ALTER TABLE wazeccp_alerts_old_y2022 RENAME TO wazeccp_alerts_y2022;
ALTER TABLE wazeccp_alerts_old_y2023 RENAME TO wazeccp_alerts_y2023;
ALTER TABLE wazeccp_alerts_old_y2024 RENAME TO wazeccp_alerts_y2024;
ALTER TABLE wazeccp_alerts_old_y2025 RENAME TO wazeccp_alerts_y2025;
ALTER TABLE wazeccp_alerts_old_y2026 RENAME TO wazeccp_alerts_y2026;
ALTER TABLE wazeccp_alerts_old_y2027max RENAME TO wazeccp_alerts_y2027mxx;

ALTER INDEX wazeccp_alerts_old_pkey RENAME TO wazeccp_alerts_pkey;
ALTER INDEX wazeccp_alerts_old_idx1 RENAME TO wazeccp_alerts_idx1;
ALTER INDEX wazeccp_alerts_old_uuid RENAME TO wazeccp_alerts_uuid;

CREATE VIEW v_waze_potholes_last_days
AS SELECT DISTINCT ON (wa.uuid) wa.uuid,
    wa.city,
    wa.location #>> '{}' as location,
    wa.street,
    wa.road_type,
    wa.magvar AS event_direction,
    timezone('Europe/Prague'::text, wa.pub_utc_date) AS published_at,
    timezone('Europe/Prague'::text, wa.created_at) AS last_reported,
    wa.reliability,
    wa.report_rating,
    wa.confidence
FROM wazeccp_alerts wa
WHERE wa.subtype = 'HAZARD_ON_ROAD_POT_HOLE'::text AND wa.pub_utc_date > (now()::date - '7 days'::interval)
ORDER BY wa.uuid, wa.pub_utc_date, wa.created_at DESC;
