CREATE VIEW v_waze_potholes_v2
as select 
	wa.*, 
	wr.name as road_type_name,	
	timezone('UTC',pub_utc_date) AS published_at,
    downloaded_at AS last_reported
from wazeccp_alerts wa
left join wazeccp_roads wr on 
	wa.road_type =  wr.id