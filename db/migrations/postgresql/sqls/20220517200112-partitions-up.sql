-- wazeccp_alerts
DROP VIEW v_waze_potholes_last_days;
DROP TABLE wazeccp_alerts;

CREATE TABLE wazeccp_alerts (
	id varchar(40) NOT NULL,
	uuid text NOT NULL,
	pub_millis int8 NOT NULL,
	pub_utc_date timestamp NULL,
	road_type int4 NULL,
	"location" jsonb NULL,
	street text NULL,
	city text NULL,
	country text NULL,
	magvar int4 NULL,
	reliability int4 NULL,
	report_description text NULL,
	report_rating int4 NULL,
	confidence int4 NULL,
	"type" text NULL,
	subtype text NULL,
	report_by_municipality_user bool NULL,
	thumbs_up int4 NULL,
	jam_uuid text NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	downloaded_at int8 NULL,
	CONSTRAINT wazeccp_alerts_pkey PRIMARY KEY (id,pub_utc_date)
) partition by range(pub_utc_date);

CREATE INDEX wazeccp_alerts_idx1 ON wazeccp_alerts USING btree (subtype, pub_utc_date);
CREATE INDEX wazeccp_alerts_uuid ON wazeccp_alerts USING btree (uuid);

CREATE TABLE wazeccp_alerts_min PARTITION OF wazeccp_alerts
    FOR VALUES FROM (MINVALUE) TO ('2019-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2019 PARTITION OF wazeccp_alerts
    FOR VALUES FROM ('2019-01-01'::timestamp) TO ('2020-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2020 PARTITION OF wazeccp_alerts
    FOR VALUES FROM ('2020-01-01'::timestamp) TO ('2021-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2021 PARTITION OF wazeccp_alerts
    FOR VALUES FROM ('2021-01-01'::timestamp) TO ('2022-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2022 PARTITION OF wazeccp_alerts
    FOR VALUES FROM ('2022-01-01'::timestamp) TO ('2023-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2023 PARTITION OF wazeccp_alerts
    FOR VALUES FROM ('2023-01-01'::timestamp) TO ('2024-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2024 PARTITION OF wazeccp_alerts
    FOR VALUES FROM ('2024-01-01'::timestamp) TO ('2025-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2025 PARTITION OF wazeccp_alerts
    FOR VALUES FROM ('2025-01-01'::timestamp) TO ('2026-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2026 PARTITION OF wazeccp_alerts
    FOR VALUES FROM ('2026-01-01'::timestamp) TO ('2027-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2027mxx PARTITION OF wazeccp_alerts
    FOR VALUES FROM ('2027-01-01'::timestamp) TO (MAXVALUE);

CREATE VIEW v_waze_potholes_last_days
AS SELECT DISTINCT ON (wa.uuid) wa.uuid,
    wa.city,
    wa.location #>> '{}' as location,
    wa.street,
    wa.road_type,
    wa.magvar AS event_direction,
    timezone('Europe/Prague'::text, wa.pub_utc_date) AS published_at,
    timezone('Europe/Prague'::text, wa.created_at) AS last_reported,
    wa.reliability,
    wa.report_rating,
    wa.confidence
   FROM wazeccp_alerts wa
  WHERE wa.subtype = 'HAZARD_ON_ROAD_POT_HOLE'::text AND wa.pub_utc_date > (now()::date - '7 days'::interval)
  ORDER BY wa.uuid, wa.pub_utc_date, wa.created_at DESC;

-- wazeccp_jams
DROP TABLE wazeccp_jams;
CREATE TABLE wazeccp_jams (
	id varchar(40) NOT NULL,
	uuid text NOT NULL,
	pub_millis int8 NOT NULL,
	pub_utc_date timestamp NULL,
	start_node text NULL,
	end_node text NULL,
	road_type int4 NULL,
	street text NULL,
	city text NULL,
	country text NULL,
	delay int4 NULL,
	speed float4 NULL,
	speed_kmh float4 NULL,
	length int4 NULL,
	turn_type text NULL,
	"level" int4 NULL,
	blocking_alert_id text NULL,
	line jsonb NULL,
	"type" text NULL,
	turn_line jsonb NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	downloaded_at int8 NULL,
	CONSTRAINT wazeccp_jams_pkey PRIMARY KEY (id,pub_utc_date)
) partition by range(pub_utc_date);

CREATE INDEX wazeccp_jams_blocking_alert_id ON wazeccp_jams USING btree (blocking_alert_id);
CREATE INDEX wazeccp_jams_created_at ON wazeccp_jams USING btree (created_at);
CREATE INDEX wazeccp_jams_downloaded_at ON wazeccp_jams USING btree (downloaded_at);
CREATE INDEX wazeccp_jams_pub_utc_date ON wazeccp_jams USING btree (pub_utc_date);


CREATE TABLE wazeccp_jams_min PARTITION OF wazeccp_jams
    FOR VALUES FROM (MINVALUE) TO ('2019-01-01'::timestamp);

CREATE TABLE wazeccp_jams_y2019 PARTITION OF wazeccp_jams
    FOR VALUES FROM ('2019-01-01'::timestamp) TO ('2020-01-01'::timestamp);

CREATE TABLE wazeccp_jams_y2020 PARTITION OF wazeccp_jams
    FOR VALUES FROM ('2020-01-01'::timestamp) TO ('2021-01-01'::timestamp);

CREATE TABLE wazeccp_jams_y2021 PARTITION OF wazeccp_jams
    FOR VALUES FROM ('2021-01-01'::timestamp) TO ('2022-01-01'::timestamp);

CREATE TABLE wazeccp_jams_y2022 PARTITION OF wazeccp_jams
    FOR VALUES FROM ('2022-01-01'::timestamp) TO ('2023-01-01'::timestamp);

CREATE TABLE wazeccp_jams_y2023 PARTITION OF wazeccp_jams
    FOR VALUES FROM ('2023-01-01'::timestamp) TO ('2024-01-01'::timestamp);

CREATE TABLE wazeccp_jams_y2024 PARTITION OF wazeccp_jams
    FOR VALUES FROM ('2024-01-01'::timestamp) TO ('2025-01-01'::timestamp);

CREATE TABLE wazeccp_jams_y2025 PARTITION OF wazeccp_jams
    FOR VALUES FROM ('2025-01-01'::timestamp) TO ('2026-01-01'::timestamp);

CREATE TABLE wazeccp_jams_y2026 PARTITION OF wazeccp_jams
    FOR VALUES FROM ('2026-01-01'::timestamp) TO ('2027-01-01'::timestamp);

CREATE TABLE wazeccp_jams_y2027mxx PARTITION OF wazeccp_jams
    FOR VALUES FROM ('2027-01-01'::timestamp) TO (MAXVALUE);
