ALTER TABLE wazeccp_alerts ADD downloaded_at bigint;
ALTER TABLE wazeccp_irregularities ADD downloaded_at bigint;
ALTER TABLE wazeccp_jams ADD downloaded_at bigint;
