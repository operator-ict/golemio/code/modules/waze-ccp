-- wazeccp_alerts_old
CREATE TABLE wazeccp_alerts_old (
	id varchar(40) NOT NULL,
	uuid text NOT NULL,
	pub_millis int8 NOT NULL,
	pub_utc_date timestamp NOT NULL,
	road_type int4 NULL,
	"location" jsonb NULL,
	street text NULL,
	city text NULL,
	country text NULL,
	magvar int4 NULL,
	reliability int4 NULL,
	report_description text NULL,
	report_rating int4 NULL,
	confidence int4 NULL,
	"type" text NULL,
	subtype text NULL,
	report_by_municipality_user bool NULL,
	thumbs_up int4 NULL,
	jam_uuid text NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	downloaded_at int8 NULL,
	CONSTRAINT wazeccp_alerts_old_pkey PRIMARY KEY (id, pub_utc_date)
)
PARTITION BY RANGE (pub_utc_date);
CREATE INDEX wazeccp_alerts_old_idx1 ON ONLY wazeccp_alerts_old USING btree (subtype, pub_utc_date);
CREATE INDEX wazeccp_alerts_old_uuid ON ONLY wazeccp_alerts_old USING btree (uuid);

CALL meta.create_yearly_partitions('wazeccp_alerts_old', '2019-01-01'::date, '2027-01-01'::date);


-- wazeccp_alerts_backup
CREATE TABLE wazeccp_alerts_backup (
	"uuid" text NOT NULL,
	pub_millis int8 NOT NULL,
	pub_utc_date timestamp NOT NULL,
	road_type int4 NULL,
	"location" jsonb NULL,
	street text NULL,
	city text NULL,
	country text NULL,
	magvar int4 NULL,
	reliability int4 NULL,
	report_description text NULL,
	report_rating int4 NULL,
	confidence int4 NULL,
	"type" text NULL,
	subtype text NULL,
	report_by_municipality_user bool NULL,
	thumbs_up int4 NULL,
	jam_uuid text NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	valid_from int8 NOT NULL,
	downloaded_at int8 NOT NULL,
	CONSTRAINT wazeccp_alerts_backup_pkey PRIMARY KEY (uuid, pub_utc_date, valid_from)
)
PARTITION BY RANGE (pub_utc_date);
CREATE INDEX wazeccp_alerts_backup_downloaded_at_idx ON ONLY wazeccp_alerts_backup USING btree (downloaded_at);
CREATE INDEX wazeccp_alerts_backup_idx ON ONLY wazeccp_alerts_backup USING btree (subtype, pub_utc_date);

CALL meta.create_yearly_partitions('wazeccp_alerts_backup', '2019-01-01'::date, '2027-01-01'::date);


-- wazeccp_alerts_alternative
CREATE TABLE wazeccp_alerts_alternative (
	"uuid" text NOT NULL,
	pub_millis int8 NOT NULL,
	pub_utc_date timestamp NOT NULL,
	road_type int4 NULL,
	"location" jsonb NULL,
	street text NULL,
	city text NULL,
	country text NULL,
	magvar int4 NULL,
	reliability int4 NULL,
	report_description text NULL,
	report_rating int4 NULL,
	confidence int4 NULL,
	"type" text NULL,
	subtype text NULL,
	report_by_municipality_user bool NULL,
	thumbs_up int4 NULL,
	jam_uuid text NULL,
	valid_from timestamptz NOT NULL,
	downloaded_at timestamptz NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT wazeccp_alerts_alternative_pkey PRIMARY KEY (uuid, pub_utc_date, valid_from)
)
PARTITION BY RANGE (pub_utc_date);
CREATE INDEX wazeccp_alerts_alternative_downloaded_at_idx ON ONLY wazeccp_alerts_alternative USING btree (downloaded_at);
CREATE INDEX wazeccp_alerts_alternative_idx ON ONLY wazeccp_alerts_alternative USING btree (subtype, pub_utc_date);

CALL meta.create_yearly_partitions('wazeccp_alerts_alternative', '2019-01-01'::date, '2027-01-01'::date);


-- wazeccp_irregularities_backup
CREATE TABLE wazeccp_irregularities_backup (
	id varchar(40) NOT NULL,
	"uuid" text NOT NULL,
	detection_date_millis int8 NOT NULL,
	detection_date text NULL,
	detection_utc_date timestamp NULL,
	update_date_millis int8 NOT NULL,
	update_date text NULL,
	update_utc_date timestamp NULL,
	street text NULL,
	city text NULL,
	country text NULL,
	is_highway bool NULL,
	speed float4 NULL,
	regular_speed float4 NULL,
	delay_seconds int4 NULL,
	seconds int4 NULL,
	length int4 NULL,
	trend int4 NULL,
	"type" text NULL,
	severity float4 NULL,
	jam_level int4 NULL,
	drivers_count int4 NULL,
	alerts_count int4 NULL,
	n_thumbs_up int4 NULL,
	n_comments int4 NULL,
	n_images int4 NULL,
	line jsonb NULL,
	cause_type text NULL,
	start_node text NULL,
	end_node text NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	downloaded_at int8 NULL,
	CONSTRAINT wazeccp_irregularities_backup_pkey PRIMARY KEY (id)
);


-- wazeccp_jams_backup
CREATE TABLE wazeccp_jams_backup (
	id varchar(40) NOT NULL,
	"uuid" text NOT NULL,
	pub_millis int8 NOT NULL,
	pub_utc_date timestamp NOT NULL,
	start_node text NULL,
	end_node text NULL,
	road_type int4 NULL,
	street text NULL,
	city text NULL,
	country text NULL,
	delay int4 NULL,
	speed float4 NULL,
	speed_kmh float4 NULL,
	length int4 NULL,
	turn_type text NULL,
	"level" int4 NULL,
	blocking_alert_id text NULL,
	line jsonb NULL,
	"type" text NULL,
	turn_line jsonb NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	downloaded_at int8 NULL,
	CONSTRAINT wazeccp_jams_backup_pkey PRIMARY KEY (id, pub_utc_date)
)
PARTITION BY RANGE (pub_utc_date);
CREATE INDEX wazeccp_jams_backup_blocking_alert_id ON ONLY wazeccp_jams_backup USING btree (blocking_alert_id);
CREATE INDEX wazeccp_jams_backup_created_at ON ONLY wazeccp_jams_backup USING btree (created_at);
CREATE INDEX wazeccp_jams_backup_downloaded_at ON ONLY wazeccp_jams_backup USING btree (downloaded_at);
CREATE INDEX wazeccp_jams_backup_pub_utc_date ON ONLY wazeccp_jams_backup USING btree (pub_utc_date);

CALL meta.create_yearly_partitions('wazeccp_jams_backup', '2019-01-01'::date, '2027-01-01'::date);
