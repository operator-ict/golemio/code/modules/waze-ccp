-- wazeccp_irregularities -> wazeccp_irregularities_backup
ALTER TABLE wazeccp_irregularities RENAME TO wazeccp_irregularities_backup;

ALTER INDEX wazeccp_irregularities_pkey RENAME TO wazeccp_irregularities_backup_pkey;

-- wazeccp_irregularities
CREATE TABLE wazeccp_irregularities (
	id varchar(40) NOT NULL,
	uuid text NOT NULL,
	detection_date_millis int8 NOT NULL,
	detection_date text NULL,
	detection_utc_date timestamp NULL,
	update_date_millis int8 NOT NULL,
	update_date text NULL,
	update_utc_date timestamp NULL,
	street text NULL,
	city text NULL,
	country text NULL,
	is_highway bool NULL,
	speed float4 NULL,
	regular_speed float4 NULL,
	delay_seconds int4 NULL,
	seconds int4 NULL,
	length int4 NULL,
	trend int4 NULL,
	"type" text NULL,
	severity float4 NULL,
	jam_level int4 NULL,
	drivers_count int4 NULL,
	alerts_count int4 NULL,
	n_thumbs_up int4 NULL,
	n_comments int4 NULL,
	n_images int4 NULL,
	line jsonb NULL,
	cause_type text NULL,
	start_node text NULL,
	end_node text NULL,
	downloaded_at timestamptz NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT wazeccp_irregularities_pkey PRIMARY KEY (id)
);


-- wazeccp_jams -> wazeccp_jams_backup
ALTER TABLE wazeccp_jams RENAME TO wazeccp_jams_backup;
ALTER TABLE wazeccp_jams_min RENAME TO wazeccp_jams_backup_min;
ALTER TABLE wazeccp_jams_y2019 RENAME TO wazeccp_jams_backup_y2019;
ALTER TABLE wazeccp_jams_y2020 RENAME TO wazeccp_jams_backup_y2020;
ALTER TABLE wazeccp_jams_y2021 RENAME TO wazeccp_jams_backup_y2021;
ALTER TABLE wazeccp_jams_y2022 RENAME TO wazeccp_jams_backup_y2022;
ALTER TABLE wazeccp_jams_y2023 RENAME TO wazeccp_jams_backup_y2023;
ALTER TABLE wazeccp_jams_y2024 RENAME TO wazeccp_jams_backup_y2024;
ALTER TABLE wazeccp_jams_y2025 RENAME TO wazeccp_jams_backup_y2025;
ALTER TABLE wazeccp_jams_y2026 RENAME TO wazeccp_jams_backup_y2026;
ALTER TABLE wazeccp_jams_y2027mxx RENAME TO wazeccp_jams_backup_y2027max;

ALTER INDEX wazeccp_jams_pkey RENAME TO wazeccp_jams_backup_pkey;
ALTER INDEX wazeccp_jams_blocking_alert_id RENAME TO wazeccp_jams_backup_blocking_alert_id;
ALTER INDEX wazeccp_jams_created_at RENAME TO wazeccp_jams_backup_created_at;
ALTER INDEX wazeccp_jams_downloaded_at RENAME TO wazeccp_jams_backup_downloaded_at;
ALTER INDEX wazeccp_jams_pub_utc_date RENAME TO wazeccp_jams_backup_pub_utc_date;

-- wazeccp_jams
CREATE TABLE wazeccp_jams (
	id varchar(40) NOT NULL,
	uuid text NOT NULL,
	pub_millis int8 NOT NULL,
	pub_utc_date timestamp NOT NULL,
	start_node text NULL,
	end_node text NULL,
	road_type int4 NULL,
	street text NULL,
	city text NULL,
	country text NULL,
	delay int4 NULL,
	speed float4 NULL,
	speed_kmh float4 NULL,
	length int4 NULL,
	turn_type text NULL,
	"level" int4 NULL,
	blocking_alert_id text NULL,
	line jsonb NULL,
	"type" text NULL,
	turn_line jsonb NULL,
	downloaded_at timestamptz NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT wazeccp_jams_pkey PRIMARY KEY (id, pub_utc_date)
) PARTITION BY RANGE (pub_utc_date);

CREATE INDEX wazeccp_jams_blocking_alert_id ON wazeccp_jams USING btree (blocking_alert_id);
CREATE INDEX wazeccp_jams_created_at ON wazeccp_jams USING btree (created_at);
CREATE INDEX wazeccp_jams_downloaded_at ON wazeccp_jams USING btree (downloaded_at);
CREATE INDEX wazeccp_jams_pub_utc_date ON wazeccp_jams USING btree (pub_utc_date);

CREATE TABLE wazeccp_jams_min PARTITION OF wazeccp_jams FOR VALUES FROM (MINVALUE) TO ('2019-01-01'::timestamp);

-- 2019 --
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2019_m01 PARTITION OF wazeccp_jams FOR VALUES FROM ('2019-01-01'::timestamp) TO ('2019-02-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2019_m02 PARTITION OF wazeccp_jams FOR VALUES FROM ('2019-02-01'::timestamp) TO ('2019-03-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2019_m03 PARTITION OF wazeccp_jams FOR VALUES FROM ('2019-03-01'::timestamp) TO ('2019-04-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2019_m04 PARTITION OF wazeccp_jams FOR VALUES FROM ('2019-04-01'::timestamp) TO ('2019-05-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2019_m05 PARTITION OF wazeccp_jams FOR VALUES FROM ('2019-05-01'::timestamp) TO ('2019-06-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2019_m06 PARTITION OF wazeccp_jams FOR VALUES FROM ('2019-06-01'::timestamp) TO ('2019-07-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2019_m07 PARTITION OF wazeccp_jams FOR VALUES FROM ('2019-07-01'::timestamp) TO ('2019-08-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2019_m08 PARTITION OF wazeccp_jams FOR VALUES FROM ('2019-08-01'::timestamp) TO ('2019-09-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2019_m09 PARTITION OF wazeccp_jams FOR VALUES FROM ('2019-09-01'::timestamp) TO ('2019-10-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2019_m10 PARTITION OF wazeccp_jams FOR VALUES FROM ('2019-10-01'::timestamp) TO ('2019-11-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2019_m11 PARTITION OF wazeccp_jams FOR VALUES FROM ('2019-11-01'::timestamp) TO ('2019-12-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2019_m12 PARTITION OF wazeccp_jams FOR VALUES FROM ('2019-12-01'::timestamp) TO ('2020-01-01'::timestamp);

-- 2020 --
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2020 PARTITION OF wazeccp_jams FOR VALUES FROM ('2020-01-01'::timestamp) TO ('2021-01-01'::timestamp);

-- 2021 --
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2021 PARTITION OF wazeccp_jams FOR VALUES FROM ('2021-01-01'::timestamp) TO ('2022-01-01'::timestamp);

-- 2022 --
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2022_m01 PARTITION OF wazeccp_jams FOR VALUES FROM ('2022-01-01'::timestamp) TO ('2022-02-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2022_m02 PARTITION OF wazeccp_jams FOR VALUES FROM ('2022-02-01'::timestamp) TO ('2022-03-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2022_m03 PARTITION OF wazeccp_jams FOR VALUES FROM ('2022-03-01'::timestamp) TO ('2022-04-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2022_m04 PARTITION OF wazeccp_jams FOR VALUES FROM ('2022-04-01'::timestamp) TO ('2022-05-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2022_m05 PARTITION OF wazeccp_jams FOR VALUES FROM ('2022-05-01'::timestamp) TO ('2022-06-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2022_m06 PARTITION OF wazeccp_jams FOR VALUES FROM ('2022-06-01'::timestamp) TO ('2022-07-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2022_m07 PARTITION OF wazeccp_jams FOR VALUES FROM ('2022-07-01'::timestamp) TO ('2022-08-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2022_m08 PARTITION OF wazeccp_jams FOR VALUES FROM ('2022-08-01'::timestamp) TO ('2022-09-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2022_m09 PARTITION OF wazeccp_jams FOR VALUES FROM ('2022-09-01'::timestamp) TO ('2022-10-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2022_m10 PARTITION OF wazeccp_jams FOR VALUES FROM ('2022-10-01'::timestamp) TO ('2022-11-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2022_m11 PARTITION OF wazeccp_jams FOR VALUES FROM ('2022-11-01'::timestamp) TO ('2022-12-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2022_m12 PARTITION OF wazeccp_jams FOR VALUES FROM ('2022-12-01'::timestamp) TO ('2023-01-01'::timestamp);

-- 2023 --
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2023_m01 PARTITION OF wazeccp_jams FOR VALUES FROM ('2023-01-01'::timestamp) TO ('2023-02-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2023_m02 PARTITION OF wazeccp_jams FOR VALUES FROM ('2023-02-01'::timestamp) TO ('2023-03-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2023_m03 PARTITION OF wazeccp_jams FOR VALUES FROM ('2023-03-01'::timestamp) TO ('2023-04-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2023_m04 PARTITION OF wazeccp_jams FOR VALUES FROM ('2023-04-01'::timestamp) TO ('2023-05-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2023_m05 PARTITION OF wazeccp_jams FOR VALUES FROM ('2023-05-01'::timestamp) TO ('2023-06-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2023_m06 PARTITION OF wazeccp_jams FOR VALUES FROM ('2023-06-01'::timestamp) TO ('2023-07-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2023_m07 PARTITION OF wazeccp_jams FOR VALUES FROM ('2023-07-01'::timestamp) TO ('2023-08-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2023_m08 PARTITION OF wazeccp_jams FOR VALUES FROM ('2023-08-01'::timestamp) TO ('2023-09-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2023_m09 PARTITION OF wazeccp_jams FOR VALUES FROM ('2023-09-01'::timestamp) TO ('2023-10-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2023_m10 PARTITION OF wazeccp_jams FOR VALUES FROM ('2023-10-01'::timestamp) TO ('2023-11-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2023_m11 PARTITION OF wazeccp_jams FOR VALUES FROM ('2023-11-01'::timestamp) TO ('2023-12-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2023_m12 PARTITION OF wazeccp_jams FOR VALUES FROM ('2023-12-01'::timestamp) TO ('2024-01-01'::timestamp);

-- 2024 --
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2024_m01 PARTITION OF wazeccp_jams FOR VALUES FROM ('2024-01-01'::timestamp) TO ('2024-02-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2024_m02 PARTITION OF wazeccp_jams FOR VALUES FROM ('2024-02-01'::timestamp) TO ('2024-03-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2024_m03 PARTITION OF wazeccp_jams FOR VALUES FROM ('2024-03-01'::timestamp) TO ('2024-04-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2024_m04 PARTITION OF wazeccp_jams FOR VALUES FROM ('2024-04-01'::timestamp) TO ('2024-05-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2024_m05 PARTITION OF wazeccp_jams FOR VALUES FROM ('2024-05-01'::timestamp) TO ('2024-06-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2024_m06 PARTITION OF wazeccp_jams FOR VALUES FROM ('2024-06-01'::timestamp) TO ('2024-07-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2024_m07 PARTITION OF wazeccp_jams FOR VALUES FROM ('2024-07-01'::timestamp) TO ('2024-08-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2024_m08 PARTITION OF wazeccp_jams FOR VALUES FROM ('2024-08-01'::timestamp) TO ('2024-09-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2024_m09 PARTITION OF wazeccp_jams FOR VALUES FROM ('2024-09-01'::timestamp) TO ('2024-10-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2024_m10 PARTITION OF wazeccp_jams FOR VALUES FROM ('2024-10-01'::timestamp) TO ('2024-11-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2024_m11 PARTITION OF wazeccp_jams FOR VALUES FROM ('2024-11-01'::timestamp) TO ('2024-12-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2024_m12 PARTITION OF wazeccp_jams FOR VALUES FROM ('2024-12-01'::timestamp) TO ('2025-01-01'::timestamp);

-- 2025 --
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2025_m01 PARTITION OF wazeccp_jams FOR VALUES FROM ('2025-01-01'::timestamp) TO ('2025-02-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2025_m02 PARTITION OF wazeccp_jams FOR VALUES FROM ('2025-02-01'::timestamp) TO ('2025-03-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2025_m03 PARTITION OF wazeccp_jams FOR VALUES FROM ('2025-03-01'::timestamp) TO ('2025-04-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2025_m04 PARTITION OF wazeccp_jams FOR VALUES FROM ('2025-04-01'::timestamp) TO ('2025-05-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2025_m05 PARTITION OF wazeccp_jams FOR VALUES FROM ('2025-05-01'::timestamp) TO ('2025-06-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2025_m06 PARTITION OF wazeccp_jams FOR VALUES FROM ('2025-06-01'::timestamp) TO ('2025-07-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2025_m07 PARTITION OF wazeccp_jams FOR VALUES FROM ('2025-07-01'::timestamp) TO ('2025-08-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2025_m08 PARTITION OF wazeccp_jams FOR VALUES FROM ('2025-08-01'::timestamp) TO ('2025-09-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2025_m09 PARTITION OF wazeccp_jams FOR VALUES FROM ('2025-09-01'::timestamp) TO ('2025-10-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2025_m10 PARTITION OF wazeccp_jams FOR VALUES FROM ('2025-10-01'::timestamp) TO ('2025-11-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2025_m11 PARTITION OF wazeccp_jams FOR VALUES FROM ('2025-11-01'::timestamp) TO ('2025-12-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2025_m12 PARTITION OF wazeccp_jams FOR VALUES FROM ('2025-12-01'::timestamp) TO ('2026-01-01'::timestamp);

-- 2026 --
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2026_m01 PARTITION OF wazeccp_jams FOR VALUES FROM ('2026-01-01'::timestamp) TO ('2026-02-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2026_m02 PARTITION OF wazeccp_jams FOR VALUES FROM ('2026-02-01'::timestamp) TO ('2026-03-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2026_m03 PARTITION OF wazeccp_jams FOR VALUES FROM ('2026-03-01'::timestamp) TO ('2026-04-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2026_m04 PARTITION OF wazeccp_jams FOR VALUES FROM ('2026-04-01'::timestamp) TO ('2026-05-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2026_m05 PARTITION OF wazeccp_jams FOR VALUES FROM ('2026-05-01'::timestamp) TO ('2026-06-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2026_m06 PARTITION OF wazeccp_jams FOR VALUES FROM ('2026-06-01'::timestamp) TO ('2026-07-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2026_m07 PARTITION OF wazeccp_jams FOR VALUES FROM ('2026-07-01'::timestamp) TO ('2026-08-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2026_m08 PARTITION OF wazeccp_jams FOR VALUES FROM ('2026-08-01'::timestamp) TO ('2026-09-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2026_m09 PARTITION OF wazeccp_jams FOR VALUES FROM ('2026-09-01'::timestamp) TO ('2026-10-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2026_m10 PARTITION OF wazeccp_jams FOR VALUES FROM ('2026-10-01'::timestamp) TO ('2026-11-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2026_m11 PARTITION OF wazeccp_jams FOR VALUES FROM ('2026-11-01'::timestamp) TO ('2026-12-01'::timestamp);
CREATE TABLE IF NOT EXISTS wazeccp_jams_y2026_m12 PARTITION OF wazeccp_jams FOR VALUES FROM ('2026-12-01'::timestamp) TO ('2027-01-01'::timestamp);

CREATE TABLE wazeccp_jams_y2027max PARTITION OF wazeccp_jams FOR VALUES FROM ('2027-01-01'::timestamp) TO (MAXVALUE);


-- wazeccp_alerts -> wazeccp_alerts_backup
ALTER TABLE wazeccp_alerts RENAME TO wazeccp_alerts_backup;
ALTER TABLE wazeccp_alerts_min RENAME TO wazeccp_alerts_backup_min;
ALTER TABLE wazeccp_alerts_y2019 RENAME TO wazeccp_alerts_backup_y2019;
ALTER TABLE wazeccp_alerts_y2020 RENAME TO wazeccp_alerts_backup_y2020;
ALTER TABLE wazeccp_alerts_y2021 RENAME TO wazeccp_alerts_backup_y2021;
ALTER TABLE wazeccp_alerts_y2022 RENAME TO wazeccp_alerts_backup_y2022;
ALTER TABLE wazeccp_alerts_y2023 RENAME TO wazeccp_alerts_backup_y2023;
ALTER TABLE wazeccp_alerts_y2024 RENAME TO wazeccp_alerts_backup_y2024;
ALTER TABLE wazeccp_alerts_y2025 RENAME TO wazeccp_alerts_backup_y2025;
ALTER TABLE wazeccp_alerts_y2026 RENAME TO wazeccp_alerts_backup_y2026;
ALTER TABLE wazeccp_alerts_y2027max RENAME TO wazeccp_alerts_backup_y2027max;

ALTER INDEX wazeccp_alerts_pkey RENAME TO wazeccp_alerts_backup_pkey;
ALTER INDEX wazeccp_alerts_idx RENAME TO wazeccp_alerts_backup_idx;
ALTER INDEX wazeccp_alerts_downloaded_at_idx RENAME TO wazeccp_alerts_backup_downloaded_at_idx;

-- wazeccp_alerts
CREATE TABLE wazeccp_alerts (
	uuid text NOT NULL,
	pub_millis int8 NOT NULL,
	pub_utc_date timestamp NULL,
	road_type int4 NULL,
	"location" jsonb NULL,
	street text NULL,
	city text NULL,
	country text NULL,
	magvar int4 NULL,
	reliability int4 NULL,
	report_description text NULL,
	report_rating int4 NULL,
	confidence int4 NULL,
	"type" text NULL,
	subtype text NULL,
	report_by_municipality_user bool NULL,
	thumbs_up int4 NULL,
	jam_uuid text NULL,
	valid_from timestamptz NOT NULL,
    downloaded_at timestamptz NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT wazeccp_alerts_pkey PRIMARY KEY (uuid,pub_utc_date,valid_from)
) partition by range(pub_utc_date);

CREATE INDEX wazeccp_alerts_idx ON wazeccp_alerts USING btree (subtype, pub_utc_date);
CREATE INDEX wazeccp_alerts_downloaded_at_idx ON wazeccp_alerts (downloaded_at);

CREATE TABLE wazeccp_alerts_min PARTITION OF wazeccp_alerts FOR VALUES FROM (MINVALUE) TO ('2019-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2019 PARTITION OF wazeccp_alerts FOR VALUES FROM ('2019-01-01'::timestamp) TO ('2020-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2020 PARTITION OF wazeccp_alerts FOR VALUES FROM ('2020-01-01'::timestamp) TO ('2021-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2021 PARTITION OF wazeccp_alerts FOR VALUES FROM ('2021-01-01'::timestamp) TO ('2022-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2022 PARTITION OF wazeccp_alerts FOR VALUES FROM ('2022-01-01'::timestamp) TO ('2023-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2023 PARTITION OF wazeccp_alerts FOR VALUES FROM ('2023-01-01'::timestamp) TO ('2024-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2024 PARTITION OF wazeccp_alerts FOR VALUES FROM ('2024-01-01'::timestamp) TO ('2025-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2025 PARTITION OF wazeccp_alerts FOR VALUES FROM ('2025-01-01'::timestamp) TO ('2026-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2026 PARTITION OF wazeccp_alerts FOR VALUES FROM ('2026-01-01'::timestamp) TO ('2027-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2027max PARTITION OF wazeccp_alerts FOR VALUES FROM ('2027-01-01'::timestamp) TO (MAXVALUE);

CREATE OR REPLACE VIEW v_waze_potholes_last_days
AS SELECT DISTINCT ON (wa.uuid) wa.uuid,
    wa.city,
    wa.location #>> '{}' as location,
    wa.street,
    wa.road_type,
    wa.magvar AS event_direction,
    timezone('Europe/Prague'::text, wa.pub_utc_date) AS published_at,
    timezone('Europe/Prague'::text, wa.created_at) AS last_reported,
    wa.reliability,
    wa.report_rating,
    wa.confidence
FROM wazeccp_alerts wa
WHERE wa.subtype = 'HAZARD_ON_ROAD_POT_HOLE'::text AND wa.pub_utc_date > (now()::date - '7 days'::interval)
ORDER BY wa.uuid, wa.pub_utc_date, wa.created_at DESC;
