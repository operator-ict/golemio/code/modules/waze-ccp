DROP TABLE IF EXISTS wazeccp_alert_types;
DROP TABLE IF EXISTS wazeccp_roads;

DROP TABLE wazeccp_alerts;

DROP TABLE wazeccp_irregularities;

DROP INDEX wazeccp_jams_blocking_alert_id;

DROP INDEX wazeccp_jams_pub_utc_date;

DROP TABLE wazeccp_jams;
