-- -- wazeccp_alerts -> wazeccp_alerts_alternative
CALL meta.rename_partitioned_table('wazeccp_alerts', 'wazeccp_alerts_alternative');

ALTER INDEX wazeccp_alerts_pkey RENAME TO wazeccp_alerts_alternative_pkey;
ALTER INDEX wazeccp_alerts_idx RENAME TO wazeccp_alerts_alternative_idx;
ALTER INDEX wazeccp_alerts_downloaded_at_idx RENAME TO wazeccp_alerts_alternative_downloaded_at_idx;

CREATE TABLE wazeccp_alerts (
	id uuid NOT NULL,
	uuid text NOT NULL,
    pub_millis int8 NOT NULL,
    pub_utc_date timestamp NULL,
    road_type int4 NULL,
    "location" jsonb NULL,
	street text NULL,
    city text NULL,
    country text NULL,
    magvar int4 NULL,
    reliability int4 NULL,
    report_description text NULL,
    report_rating int4 NULL,
    confidence int4 NULL,
    "type" text NULL,
    subtype text NULL,
    report_by_municipality_user bool NULL,
    thumbs_up int4 NULL,
    jam_uuid text NULL,
    valid_from timestamptz NOT NULL,
    downloaded_at timestamptz NOT NULL,
	duplicate_count int4 NOT NULL DEFAULT 0,
    -- 	audit
	create_batch_id int8 NULL,
    created_at timestamptz NULL,
    created_by varchar(150) NULL,
    update_batch_id int8 NULL,
    updated_at timestamptz NULL,
    updated_by varchar(150) NULL,
	CONSTRAINT wazeccp_alerts_pkey PRIMARY KEY (id,uuid,pub_utc_date)
) partition by range(pub_utc_date);

CREATE INDEX wazeccp_alerts_idx ON wazeccp_alerts USING btree (subtype, pub_utc_date);
CREATE INDEX wazeccp_alerts_downloaded_at_idx ON wazeccp_alerts (downloaded_at);

COMMENT ON COLUMN wazeccp_alerts.id IS '
uuid generated from md5 hash of columns:
pub_millis, road_type, location, street, city, country, type, subtype, report_by_municipality_user, jam_uuid
';

CALL meta.create_yearly_partitions('wazeccp_alerts', '2019-01-01'::date, '2027-01-01'::date);

CREATE OR REPLACE VIEW v_waze_potholes_last_days
AS SELECT DISTINCT ON (wa.uuid) wa.uuid,
    wa.city,
    wa.location #>> '{}' as location,
    wa.street,
    wa.road_type,
    wa.magvar AS event_direction,
    timezone('Europe/Prague'::text, wa.pub_utc_date) AS published_at,
    timezone('Europe/Prague'::text, wa.created_at) AS last_reported,
    wa.reliability,
    wa.report_rating,
    wa.confidence
FROM wazeccp_alerts wa
WHERE wa.subtype = 'HAZARD_ON_ROAD_POT_HOLE'::text AND wa.pub_utc_date > (now()::date - '7 days'::interval)
ORDER BY wa.uuid, wa.pub_utc_date, wa.created_at DESC;
