-- wazeccp_alerts -> wazeccp_alerts_old
ALTER TABLE wazeccp_alerts RENAME TO wazeccp_alerts_old;
ALTER TABLE wazeccp_alerts_min RENAME TO wazeccp_alerts_old_min;
ALTER TABLE wazeccp_alerts_y2019 RENAME TO wazeccp_alerts_old_y2019;
ALTER TABLE wazeccp_alerts_y2020 RENAME TO wazeccp_alerts_old_y2020;
ALTER TABLE wazeccp_alerts_y2021 RENAME TO wazeccp_alerts_old_y2021;
ALTER TABLE wazeccp_alerts_y2022 RENAME TO wazeccp_alerts_old_y2022;
ALTER TABLE wazeccp_alerts_y2023 RENAME TO wazeccp_alerts_old_y2023;
ALTER TABLE wazeccp_alerts_y2024 RENAME TO wazeccp_alerts_old_y2024;
ALTER TABLE wazeccp_alerts_y2025 RENAME TO wazeccp_alerts_old_y2025;
ALTER TABLE wazeccp_alerts_y2026 RENAME TO wazeccp_alerts_old_y2026;
ALTER TABLE wazeccp_alerts_y2027mxx RENAME TO wazeccp_alerts_old_y2027mxx;

ALTER INDEX wazeccp_alerts_pkey RENAME TO wazeccp_alerts_old_pkey;
ALTER INDEX wazeccp_alerts_idx1 RENAME TO wazeccp_alerts_old_idx1;
ALTER INDEX wazeccp_alerts_uuid RENAME TO wazeccp_alerts_old_uuid;

-- wazeccp_alerts
CREATE TABLE wazeccp_alerts (
	uuid text NOT NULL,
	pub_millis int8 NOT NULL,
	pub_utc_date timestamp NULL,
	road_type int4 NULL,
	"location" jsonb NULL,
	street text NULL,
	city text NULL,
	country text NULL,
	magvar int4 NULL,
	reliability int4 NULL,
	report_description text NULL,
	report_rating int4 NULL,
	confidence int4 NULL,
	"type" text NULL,
	subtype text NULL,
	report_by_municipality_user bool NULL,
	thumbs_up int4 NULL,
	jam_uuid text NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	valid_from int8 NOT NULL,
	downloaded_at int8 NOT NULL,
	CONSTRAINT wazeccp_alerts_pkey PRIMARY KEY (uuid,pub_utc_date,valid_from)
) partition by range(pub_utc_date);

CREATE INDEX wazeccp_alerts_idx ON wazeccp_alerts USING btree (subtype, pub_utc_date);
CREATE INDEX wazeccp_alerts_downloaded_at_idx ON wazeccp_alerts (downloaded_at);

CREATE TABLE wazeccp_alerts_min PARTITION OF wazeccp_alerts
    FOR VALUES FROM (MINVALUE) TO ('2019-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2019 PARTITION OF wazeccp_alerts
    FOR VALUES FROM ('2019-01-01'::timestamp) TO ('2020-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2020 PARTITION OF wazeccp_alerts
    FOR VALUES FROM ('2020-01-01'::timestamp) TO ('2021-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2021 PARTITION OF wazeccp_alerts
    FOR VALUES FROM ('2021-01-01'::timestamp) TO ('2022-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2022 PARTITION OF wazeccp_alerts
    FOR VALUES FROM ('2022-01-01'::timestamp) TO ('2023-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2023 PARTITION OF wazeccp_alerts
    FOR VALUES FROM ('2023-01-01'::timestamp) TO ('2024-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2024 PARTITION OF wazeccp_alerts
    FOR VALUES FROM ('2024-01-01'::timestamp) TO ('2025-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2025 PARTITION OF wazeccp_alerts
    FOR VALUES FROM ('2025-01-01'::timestamp) TO ('2026-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2026 PARTITION OF wazeccp_alerts
    FOR VALUES FROM ('2026-01-01'::timestamp) TO ('2027-01-01'::timestamp);

CREATE TABLE wazeccp_alerts_y2027max PARTITION OF wazeccp_alerts
    FOR VALUES FROM ('2027-01-01'::timestamp) TO (MAXVALUE);

CREATE OR REPLACE VIEW v_waze_potholes_last_days
AS SELECT DISTINCT ON (wa.uuid) wa.uuid,
    wa.city,
    wa.location #>> '{}' as location,
    wa.street,
    wa.road_type,
    wa.magvar AS event_direction,
    timezone('Europe/Prague'::text, wa.pub_utc_date) AS published_at,
    timezone('Europe/Prague'::text, wa.created_at) AS last_reported,
    wa.reliability,
    wa.report_rating,
    wa.confidence
FROM wazeccp_alerts wa
WHERE wa.subtype = 'HAZARD_ON_ROAD_POT_HOLE'::text AND wa.pub_utc_date > (now()::date - '7 days'::interval)
ORDER BY wa.uuid, wa.pub_utc_date, wa.created_at DESC;
