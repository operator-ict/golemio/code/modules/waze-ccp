import { WazeCCPIrregularitiesTransformation } from "#ie/WazeCCPIrregularitiesTransformation";
import { WazeCCP } from "#sch/index";
import { IrregularitiesDto } from "#sch/models/IrregularityDto";
import { IValidator, JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";

chai.use(chaiAsPromised);

const readFile = (file: string): Promise<Buffer> => {
    return new Promise((resolve, reject) => {
        const stream = fs.createReadStream(file);
        const chunks: any[] = [];

        stream.on("error", (err) => {
            reject(err);
        });
        stream.on("data", (data) => {
            chunks.push(data);
        });
        stream.on("close", () => {
            resolve(Buffer.concat(chunks));
        });
    });
};

describe("WazeCCPIrregularitiesTransformation", () => {
    let transformation: WazeCCPIrregularitiesTransformation;
    let testSourceData: Record<string, any>;
    let validator: IValidator;

    before(() => {
        validator = new JSONSchemaValidator(WazeCCP.irregularities.name + "ModelValidator", IrregularitiesDto.jsonSchema);
    });

    beforeEach(async () => {
        transformation = new WazeCCPIrregularitiesTransformation();
        const buffer = await readFile(__dirname + "/data/wazeccp_irregularities-datasource.json");
        testSourceData = JSON.parse(Buffer.from(buffer).toString("utf8"));
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("WazeCCPIrregularities");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform collection", async () => {
        // enrich data by timestamp of download
        const dataWithDownloadAt = { ...testSourceData, downloadedAt: new Date() };
        const data = await transformation.transform(dataWithDownloadAt);
        await expect(validator.Validate(data)).to.be.fulfilled;

        for (let i = 0, imax = data.length; i < imax; i++) {
            expect(data[i]).to.have.property("detection_date_millis");
            expect(data[i]).to.have.property("id");
            expect(data[i]).to.have.property("uuid");
            expect(data[i]).to.have.property("update_date_millis");
        }
    });
});
