import { WazeCCPAlertsTransformation } from "#ie/WazeCCPAlertsTransformation";
import { WazeCCP } from "#sch/index";
import { AlertDto } from "#sch/models/AlertDto";
import { IValidator, JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";

chai.use(chaiAsPromised);

describe("WazeCCPAlertsTransformation", () => {
    let transformation: WazeCCPAlertsTransformation;
    let testSourceData: Record<string, any>;
    let validator: IValidator;

    before(() => {
        validator = new JSONSchemaValidator(WazeCCP.alerts.name + "ModelValidator", AlertDto.jsonSchema);
    });

    beforeEach(async () => {
        transformation = new WazeCCPAlertsTransformation(new Date(1699873200000));
        testSourceData = JSON.parse(fs.readFileSync(__dirname + "/data/wazeccp_alerts-datasource.json", "utf8"));
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("WazeCCPAlertsTransformation");
    });

    it("should has transform method", async () => {
        expect(transformation.transformArray).not.to.be.undefined;
    });

    it("should properly transform collection", async () => {
        const data = transformation.transformArray(testSourceData.alerts);
        await expect(validator.Validate(data)).to.be.fulfilled;

        for (let i = 0, imax = data.length; i < imax; i++) {
            expect(data[i]).to.have.property("id");
            expect(data[i]).to.have.property("uuid");
            expect(data[i]).to.have.property("pub_utc_date");
            expect(data[i]).to.have.property("downloaded_at");
            expect(data[i]).to.have.property("pub_millis");
            expect(data[i]).to.have.property("pub_millis");
        }
    });
});
