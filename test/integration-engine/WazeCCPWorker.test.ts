import sinon, { SinonSandbox, SinonStub, SinonSpy } from "sinon";
import { expect } from "chai";
import { config } from "@golemio/core/dist/integration-engine/config";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { WazeCCP } from "#sch/index";
import { WazeCCPWorker } from "#ie/WazeCCPWorker";
import fs from "fs";

describe("WazeCCPWorker", () => {
    let worker: WazeCCPWorker;
    let sandbox: SinonSandbox;
    let sequelizeModelStub: Record<string, SinonStub>;
    let queuePrefix: string;
    let testDataAlerts: Record<string, any>;
    let testDataIrregularities: Record<string, any>;
    let testDataJams: Record<string, any>;
    let testTransformedData: number[];
    let alertsGetAll: SinonStub;

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true }); // important for `downloadedAt`
        sequelizeModelStub = Object.assign({
            hasMany: sandbox.stub(),
            removeAttribute: sandbox.stub(),
            findOne: sandbox.stub().callsFake((data) => {
                if (data.where.subtype === "duplicate") {
                    return [1, 2];
                }
                return null;
            }),
            create: sandbox.stub(),
            update: sandbox.stub(),
        });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().callsFake(() => sequelizeModelStub),
                query: sandbox.stub().callsFake(() => [true]),
                transaction: sandbox.stub().callsFake(() => Object.assign({ commit: sandbox.stub() })),
            })
        );

        testDataAlerts = {
            alerts: [1, 2],
            downloadedAt: new Date(),
            startTimeMillis: 1574245920000,
        };
        testDataIrregularities = {
            downloadedAt: new Date(),
            irregularities: [1, 2],
            startTimeMillis: 1574245920000,
        };
        testDataJams = {
            downloadedAt: new Date(),
            jams: [1, 2],
            startTimeMillis: 1574245920000,
        };
        testTransformedData = [1, 2];

        worker = new WazeCCPWorker();

        alertsGetAll = sandbox.stub(worker["dataSourceAlerts"], "getAll");

        sandbox.stub(worker["dataSourceIrregularities"], "getAll").callsFake(() => Promise.resolve(testDataIrregularities));
        sandbox.stub(worker["dataSourceJams"], "getAll").callsFake(() => Promise.resolve(testDataJams));

        sandbox.stub(worker["transformationIrregularities"], "transform").callsFake(() => Promise.resolve(testTransformedData));
        sandbox.stub(worker["transformationJams"], "transform").callsFake(() => Promise.resolve(testTransformedData));

        sandbox.stub(worker["modelIrregularities"], "saveBySqlFunction");
        sandbox.stub(worker["modelJams"], "saveBySqlFunction");

        sandbox.stub(worker, "sendMessageToExchange" as any);
        queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + WazeCCP.name.toLowerCase();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call the correct methods by refreshAlertsInDB method", async () => {
        const alertsSourceData = JSON.parse(fs.readFileSync(__dirname + "/data/wazeccp_alerts-datasource.json", "utf8"));
        const data = { ...alertsSourceData, alerts: alertsSourceData.alerts.slice(0, 2) };
        alertsGetAll.callsFake(() => Promise.resolve(data));
        await worker.refreshAlertsInDB({});
        sandbox.assert.calledOnce(worker["dataSourceAlerts"].getAll as SinonSpy);
        sandbox.assert.calledTwice(sequelizeModelStub.create as SinonSpy);
        sandbox.assert.callOrder(alertsGetAll as SinonSpy, sequelizeModelStub.create as SinonSpy);
    });

    it("should count duplicate alerts", async () => {
        sequelizeModelStub.findOne.onFirstCall().returns(Promise.resolve(null));
        sequelizeModelStub.findOne.onSecondCall().returns(Promise.resolve({ duplicate_count: 0 }));
        const alertsSourceData = JSON.parse(fs.readFileSync(__dirname + "/data/wazeccp_alerts-datasource.json", "utf8"));
        const data = { ...alertsSourceData, alerts: alertsSourceData.alerts.slice(0, 2) };
        alertsGetAll.callsFake(() => Promise.resolve(data));
        await worker.refreshAlertsInDB({});
        sandbox.assert.calledOnce(sequelizeModelStub.create);
        expect(sequelizeModelStub.create.firstCall.args[0]).to.have.property("duplicate_count").that.equals(0);
        sandbox.assert.calledOnce(sequelizeModelStub.update);
        expect(sequelizeModelStub.update.firstCall.args[0]).to.have.property("duplicate_count").that.equals(1);
    });

    it("should call the correct methods by refreshIrregularitiesInDB method", async () => {
        await worker.refreshIrregularitiesInDB({});
        sandbox.assert.calledOnce(worker["dataSourceIrregularities"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["transformationIrregularities"].transform as SinonSpy);
        sandbox.assert.calledWith(worker["transformationIrregularities"].transform as SinonSpy, testDataIrregularities);
        sandbox.assert.calledOnce(worker["modelIrregularities"].saveBySqlFunction as SinonSpy);
        sandbox.assert.callOrder(
            worker["dataSourceIrregularities"].getAll as SinonSpy,
            worker["transformationIrregularities"].transform as SinonSpy,
            worker["modelIrregularities"].saveBySqlFunction as SinonSpy
        );
    });

    it("should call the correct methods by refreshJamsInDB method", async () => {
        await worker.refreshJamsInDB({});
        sandbox.assert.calledOnce(worker["dataSourceJams"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["transformationJams"].transform as SinonSpy);
        sandbox.assert.calledWith(worker["transformationJams"].transform as SinonSpy, testDataJams);
        sandbox.assert.calledOnce(worker["modelJams"].saveBySqlFunction as SinonSpy);
        sandbox.assert.callOrder(
            worker["dataSourceJams"].getAll as SinonSpy,
            worker["transformationJams"].transform as SinonSpy,
            worker["modelJams"].saveBySqlFunction as SinonSpy
        );
    });

    it("should call the correct methods by refreshAllDataInDB method", async () => {
        await worker.refreshAllDataInDB({});
        sandbox.assert.callCount(worker["sendMessageToExchange"] as SinonSpy, 3);
        ["refreshAlertsInDB", "refreshIrregularitiesInDB", "refreshJamsInDB"].map((f) => {
            sandbox.assert.calledWith(
                worker["sendMessageToExchange"] as SinonSpy,
                "workers." + queuePrefix + "." + f,
                "Just do it!"
            );
        });
    });
});
