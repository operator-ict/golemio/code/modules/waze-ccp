import { PotHolesContainer } from "#og/ioc/Di";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import request from "supertest";

chai.use(chaiAsPromised);

describe("Pot holes router", () => {
    const app = express();

    before(async () => {
        await PotHolesContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector).connect();

        const { v1PotHolesRouter } = require("#og/routers/PotHolesRouter");
        app.use(v1PotHolesRouter.getPath(), v1PotHolesRouter.getRouter());

        app.use("/potholes", v1PotHolesRouter.getRouter());
        app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond with json to GET /potholes", (done) => {
        const expectedResponse = fs.readFileSync(__dirname + "/../../data/potholes-data-og-output.json", "utf8");
        request(app)
            .get("/potholes/data?from=2023-11-19T00:00:00.000Z&to=2023-11-22T23:59:59.999Z")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response: any) => {
                expect(response.body).to.deep.eq(JSON.parse(expectedResponse));
                expect(response.headers["cache-control"]).to.equal("public, s-maxage=180, stale-while-revalidate=60");
                done();
            })
            .catch((err) => done(err));
    });
});
