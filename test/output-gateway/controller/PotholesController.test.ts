import { PotholesController } from "#og/controllers/PotholesController";
import { IPotholesController } from "#og/domain/IPotholesController";
import { IPotholesParams } from "#og/domain/IPotholesParams";
import { IPotholesRepository } from "#og/domain/IPotholesRepository";
import { ModuleContainerToken } from "#og/ioc/ModuleContainerToken";
import { PotholesTransformation } from "#og/transformation/PotholesDtoTransformation";
import { IAlertDto } from "#sch/interfaces/IAlertDto";
import { IGeoJSONFeatureCollection, buildGeojsonFeatureCollection } from "@golemio/core/dist/output-gateway";
import { Request, Response } from "@golemio/core/dist/shared/express";
import { container } from "@golemio/core/dist/shared/tsyringe";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import sinon, { SinonSpy } from "sinon";

chai.use(chaiAsPromised);

class MockRepository implements IPotholesRepository {
    constructor(private testSourceData: IAlertDto[]) {}

    GetAll(params: Partial<IPotholesParams>) {
        return Promise.resolve(this.testSourceData);
    }
    GetOne(id: string): Promise<IAlertDto> {
        throw new Error("Method not implemented.");
    }
}

describe("PotholesController", () => {
    const testContainer = container.createChildContainer();
    let sandbox: sinon.SinonSandbox;
    let testSourceData: IAlertDto[];
    let mockRepository: IPotholesRepository;

    before(() => {
        sandbox = sinon.createSandbox();

        testSourceData = JSON.parse(fs.readFileSync(__dirname + "/../data/wazeccp_alerts-fromdb.json", "utf8"));
    });

    beforeEach(async () => {
        mockRepository = new MockRepository(testSourceData);
        sandbox.restore();
        sinon.spy(mockRepository, "GetAll");
        testContainer.clearInstances();
        testContainer.registerSingleton<PotholesTransformation>(
            ModuleContainerToken.PotholesTransformation,
            PotholesTransformation
        );
        testContainer.registerInstance(ModuleContainerToken.PotholesRepository, mockRepository);

        testContainer.registerSingleton<IPotholesController>(ModuleContainerToken.PotholesController, PotholesController);
    });

    it("should have getAll method", async () => {
        const controller = testContainer.resolve<PotholesController>(ModuleContainerToken.PotholesController);
        expect(controller.getAll).not.to.be.undefined;
    });

    it("should properly transform collection", async () => {
        const repository = testContainer.resolve<IPotholesRepository>(ModuleContainerToken.PotholesRepository);
        const requestMock = {
            query: {
                limit: "100",
                offset: "10",
                from: "2021-01-01T00:00:00Z",
                to: "2021-01-01T00:00:00Z",
            },
        };
        const next = sinon.stub();
        const controller = testContainer.resolve<IPotholesController>(ModuleContainerToken.PotholesController);
        const expectedOutput = JSON.parse(fs.readFileSync(__dirname + "/../data/wazeccp_alerts-expectedApiOutput.json", "utf8"));
        let result;
        await controller.getAll(
            requestMock as unknown as Request,
            {
                json: (resultData: IGeoJSONFeatureCollection) => {
                    result = resultData;
                },
            } as unknown as Response,
            next
        );

        expect(next.called).to.be.false;
        expect(result).to.be.deep.equal(buildGeojsonFeatureCollection(expectedOutput));
        expect(
            (mockRepository.GetAll as SinonSpy).calledOnceWith({
                limit: 100,
                offset: 10,
                dateFrom: "2021-01-01T00:00:00Z",
                dateTo: "2021-01-01T00:00:00Z",
            })
        ).to.be.true;
    });
});
