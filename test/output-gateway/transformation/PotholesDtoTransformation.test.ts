import { IPothole } from "#og/domain/IPothole";
import { PotholesTransformation } from "#og/transformation/PotholesDtoTransformation";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";

chai.use(chaiAsPromised);

describe("PotholesTransformation", () => {
    let transformation: PotholesTransformation;
    let testSourceData: IPothole[];

    beforeEach(async () => {
        transformation = new PotholesTransformation();
        testSourceData = JSON.parse(fs.readFileSync(__dirname + "/../data/wazeccp_alerts-fromdb.json", "utf8"));
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("PotholesTransformation");
    });

    it("should have transform method", async () => {
        expect(transformation.transformArray).not.to.be.undefined;
    });

    it("should properly transform collection", async () => {
        const expectedOutput = JSON.parse(fs.readFileSync(__dirname + "/../data/wazeccp_alerts-expectedApiOutput.json", "utf8"));
        const transformedData = transformation.transformArray(testSourceData);

        expect(transformedData).to.be.deep.equal(expectedOutput);
    });
});
